package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductAttributeValuesTest.AttributeValuesItem;
import com.example.JsonObjects.ProductAttributeValuesTest.ProductAttributeValues;
import com.example.JsonObjects.ProductAttributeValuesTest.DeletedItem;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/14/15.
 */
public class ProductAttributeValuesTest {
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd1dfbhgik4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductAttributeValuesTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/attributeValues", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductAttributeValues productAttributeValues =
                    new ResponseMapper().map(content, ProductAttributeValues.class);

            assertTrue("Status field incorrect", productAttributeValues.getStatus().equals("OK")
                    || productAttributeValues.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productAttributeValues.getItems());

            if (productAttributeValues != null) {

                for (AttributeValuesItem attributeValuseItem : productAttributeValues.getItems()) {
                    assertNotNull("id filed is null", attributeValuseItem.getId());
                    assertNotNull("Attribute id field is null", attributeValuseItem.getAttributeId());
                    /*if (TextUtils.isEmpty(attributeValuseItem.getName())) {
                    System.out.print(attributeValuseItem.getId()+ "," + " ");
                    }*/
                    assertFalse("Name field error", TextUtils.isEmpty(attributeValuseItem.getName()));
                }

                for (DeletedItem deletedItem : productAttributeValues.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", productAttributeValues.getzDuration());
                //czy nie jest strasznie duże to duration, ale to potem
                assertNotNull("TimeStamp field is null", productAttributeValues.getTime());
                assertTrue("Records field incorrect", productAttributeValues.getItems().size() == productAttributeValues.getRecords());
            } else {
                fail("Products is null");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductAttributeValuesTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/attributeValues", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void RandomAttributeValuesTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/attributeValues", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
