package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductInspirationTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class ProductInspirationTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd1dsjgj411e4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductInspirationTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/inspiration", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductInspiration productInspiration = new ResponseMapper().map(content, ProductInspiration.class);

            assertTrue("Status field incorrect", productInspiration.getStatus().equals("OK")
                    || productInspiration.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productInspiration.getItems());

            if (productInspiration != null) {

                for (ProductInspirationItem item : productInspiration.getItems()) {

                    assertNotNull("id field is null", item.getId());
                    assertNotNull("retailerId field is null", item.getRetailerId());
                    assertNotNull("name field is null", item.getName());

                    for (ProductInspirationElement element : item.getList()) {
                        assertNotNull("element id field is null", element.getId());
                        assertFalse("element name field is null", TextUtils.isEmpty(element.getImage()));
                        assertNotNull("element position field is null", element.getPosition());
                        assertNotNull("element tab_id field is null", element.getTabId());
                        assertNotNull("element valueId field is null", element.getValueId());
                        assertFalse("element name field is null", TextUtils.isEmpty(element.getName()));
                    }

                }
                for (DeletedItem deletedItem : productInspiration.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", productInspiration.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", productInspiration.getTime());
                assertTrue("Records field incorrect", productInspiration.getItems().size() == productInspiration.getRecords());

            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductInspirationTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/inspiration", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ProductInspirationTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/inspiration", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}

