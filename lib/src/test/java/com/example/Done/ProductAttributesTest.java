package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductAttributesTest.AttributeItem;
import com.example.JsonObjects.ProductAttributesTest.DeletedItem;
import com.example.JsonObjects.ProductAttributesTest.ProductAttributes;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 12/31/14.
 */
public class ProductAttributesTest {
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd120fghje";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductAttributesTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/attributes", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductAttributes productAttributes = new ResponseMapper().map(content, ProductAttributes.class);
            assertTrue("Status field incorrect", productAttributes.getStatus().equals("OK")
                    || productAttributes.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productAttributes.getItems());

            if (productAttributes != null) {
                for (AttributeItem attributeItem : productAttributes.getItems()) {
                    assertNotNull("Id field is null", attributeItem.getId());
                    assertNotNull("Symbol field is null", attributeItem.getSymbol());
                    System.out.println(attributeItem.getPosition());
                    assertNotNull("Position field is null", attributeItem.getPosition());
                    assertNotNull("name field is null", attributeItem.getName());
                    assertNotNull("AttributeType field is null", attributeItem.getAttributeType());
                }

                for (DeletedItem deletedItem : productAttributes.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }

                assertNotNull("zDuration field is null", productAttributes.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", productAttributes.getTime());
                assertTrue("Records field incorrect", productAttributes.getItems().size() == productAttributes.getRecords());

            } else {
                fail("Products is null");
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductAttributesTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/attributes", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void RandomAttributesTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/attributes", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
