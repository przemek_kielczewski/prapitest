package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.Randomizer;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class HeartBeatTest {
        Randomizer randomizer = new Randomizer();
        ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
        ConnectionMaker connectionMaker = new ConnectionMaker();

    //tests

    @Test//urządzenie z mojego biurka, dane logowania to przemek/przemek
    public void CorrectHeartbeatTest1() throws IOException {
            keyValue.add(new BasicNameValuePair("api_key", "8b8569745eb411e49929"));
            keyValue.add(new BasicNameValuePair("registration_id",
                    "pQpIoVjVXvybpKuZ3PZxGko4PfCB2QEuM3Garg7ea8XUIXA8hHyGoEBRvB7CrxqIAI6HETx_00EOUxxsT9IX65bshP2CqHaVckL-8iTpOi4gz6naFQiEfl83TwzZ6g3ztM6O_31l33bpcqJyTcn2gCQ&"));
            keyValue.add(new BasicNameValuePair("data",
                    "{\"user_info\" : {\n" +
                            "        \"login\" : \"przemek\",\n" +
                            "        \"api_key\" : \"8b8569745eb411e49929\",\n" +
                            "        \"registration_id_md5\" : \"ae06549f44bd281d63d34d8968ff4c5e\"\n" +
                            "    },\n" +
                            "    \"device_uptime\" : {\n" +
                            "        \"uptime\" : \"454275\",\n" +
                            "        \"boot\" : \"1421401788\"\n" +
                            "    },\n" +
                            "    \"ram_info\" : {\n" +
                            "        \"total\" : \"44720128\",\n" +
                            "        \"free\" : \"27340192\"\n" +
                            "    },\n" +
                            "    \"app\" : {\n" +
                            "        \"android_version\" : \"4.2.2\",\n" +
                            "        \"version_name\" : \"1.14.07-K\",\n" +
                            "        \"model\" : \"OUTFORM\",\n" +
                            "        \"is_release\" : true,\n" +
                            "        \"app_running\" : true,\n" +
                            "        \"version_code\" : 83\n" +
                            "    },\n" +
                            "    \"location\" : {\n" +
                            "        \"longnitude\" : \"-1.0\",\n" +
                            "        \"latitude\" : \"-1.0\"\n" +
                            "    },\n" +
                            "    \"connection_info\" : {\n" +
                            "        \"connection_type\" : \"WIFI\"\n" +
                            "    },\n" +
                            "    \"cpu\" : {\n" +
                            "        \"current\" : 0.0010613405611366034\n" +
                            "    },\n" +
                            "    \"last_updates\" : {\n" +
                            "        \"it\" : \"1421412449\"\n" +
                            "    },\n" +
                            "    \"storage\" : {\n" +
                            "        \"\\/storage\\/emulated\\/0\" : {\n" +
                            "            \"total\" : 28361302016,\n" +
                            "            \"free\" : 26784137216\n" +
                            "        }\n" +
                            "    }\n" +
                            "}"));
            String content = connectionMaker.Connect
                    ("http://app.peak-retail.com/web/api.php/event/heartbeat", keyValue);

            System.out.println(content);
            try {
                    ApiResponse apiResponse = new ResponseMapper().map(content, ApiResponse.class);
                    assertTrue("Status field incorrect", apiResponse.getStatus().equals("OK"));
            } catch (IOException e2) {
                    e2.printStackTrace();
                    fail("parsing error");
            }
    }

    @Test//urządzenie z mojego biurka, dane logowania to przemek/przemek
    public void TypoHeartbeatTest2() throws IOException {
            keyValue.add(new BasicNameValuePair("api_key", "8b8569745eb411e49929"));
            keyValue.add(new BasicNameValuePair("registration_id",
                    "pQpIoVjVXvybpKuZ3PZxGko4PfCB2QEuM3Garg7ea8XUIXA8hHyGoEBRvB7CrxqIAI6HETx_00EOUxxsT9IX65bshP2CqHaVckL-8iTpOi4gz6naFQiEfl83TwzZ6g3ztM6O_31l33bpcqJyTcn2gCQ&"));
            keyValue.add(new BasicNameValuePair("data",
                    "{\"user_info\" : {\n" +
                            "        \"logn\" : \"przenek\",\n" + //tutaj
                            "        \"api_key\" : \"8b8569745eb411e49929\",\n" +
                            "        \"registration_id_md5\" : \"ae06549f44bd281d63d34d8968ff4c5e\"\n" +
                            "    },\n" +
                            "    \"device_uptime\" : {\n" +
                            "        \"uptime\" : \"454275\",\n" +
                            "        \"boot\" : \"1421401788\"\n" +
                            "    },\n" +
                            "    \"ram_info\" : {\n" +
                            "        \"total\" : \"44720128\",\n" +
                            "        \"free\" : \"27340192\"\n" +
                            "    },\n" +
                            "    \"app\" : {\n" +
                            "        \"android_version\" : \"4.2.2\",\n" +
                            "        \"version_name\" : \"1.14.07-K\",\n" +
                            "        \"moel\" : \"OUTFRM\",\n" + //tutaj
                            "        \"is_rease\" : true,\n" + //tutaj
                            "        \"app_running\" : true,\n" +
                            "        \"version_code\" : 83\n" +
                            "    },\n" +
                            "    \"location\" : {\n" +
                            "        \"longnitude\" : \"-1.0\",\n" +
                            "        \"latitude\" : \"-1.0\"\n" +
                            "    },\n" +
                            "    \"connection_info\" : {\n" +
                            "        \"connection_    type\" : \"WIFI\"\n" + //tutaj
                            "    },\n" +
                            "    \"cpu\" : {\n" +
                            "        \"current\" : 0.0010613405611366034\n" +
                            "    },\n" +
                            "    \"last_   updates\" : {\n" + //tutaj
                            "        \"it\" : \"1421412449\"\n" +
                            "    },\n" +
                            "    \"storage\" : {\n" +
                            "        \"\\/storage\\/emulated\\/0\" : {\n" +
                            "            \"total\" : 28361302016,\n" +
                            "            \"free\" : 26784137216\n" +
                            "        }\n" +
                            "    }\n" +
                            "}"));
            String content = connectionMaker.Connect
                    ("http://app.peak-retail.com/web/api.php/event/heartbeat", keyValue);

            System.out.println(content);
            try {
                    ApiResponse apiResponse = new ResponseMapper().map(content, ApiResponse.class);
                    assertTrue("Status field incorrect", apiResponse.getStatus().equals("OK") ||
                            apiResponse.getStatus().equals("ERROR"));

            } catch (IOException e2) {
                    e2.printStackTrace();
                    fail("parsing error");
            }
    }

        @Test//urządzenie z mojego biurka, dane logowania to przemek/przemek
        public void TypoHeartbeatTest3() throws IOException {
                keyValue.add(new BasicNameValuePair("api_key", "8b8569745eb411e49929"));
                keyValue.add(new BasicNameValuePair("registration_id",
                        "pQpIoVjVXvybpKuZ3PZxGko4PfCB2QEuM3Garg7ea8XUIXA8hHyGoEBRvB7CrxqIAI6HETx_00EOUxxsT9IX65bshP2CqHaVckL-8iTpOi4gz6naFQiEfl83TwzZ6g3ztM6O_31l33bpcqJyTcn2gCQ&"));
                keyValue.add(new BasicNameValuePair("data",
                        "{\"us_inf\" : {\n" +//zmień tu to się wysypie, gdzie indziej nie.
                                "        \"login\" : \"przemek\",\n" +
                                "        \"api_key\" : \"8b8569745eb411e49929\",\n" +
                                "        \"registration_id_md5\" : \"ae06549f44bd281d63d34d8968ff4c5e\"\n" +
                                "    },\n" +
                                "    \"device_uptime\" : {\n" +
                                "        \"uptime\" : \"454275\",\n" +
                                "        \"boot\" : \"1421401788\"\n" +
                                "    },\n" +
                                "    \"ram_info\" : {\n" +
                                "        \"total\" : \"44720128\",\n" +
                                "        \"free\" : \"27340192\"\n" +
                                "    },\n" +
                                "    \"app\" : {\n" +
                                "        \"android_version\" : \"4.2.2\",\n" +
                                "        \"version_name\" : \"1.14.07-K\",\n" +
                                "        \"model\" : \"OUTFORM\",\n" +
                                "        \"is_release\" : true,\n" +
                                "        \"app_running\" : true,\n" +
                                "        \"version_code\" : 83\n" +
                                "    },\n" +
                                "    \"location\" : {\n" +
                                "        \"longnitude\" : \"-1.0\",\n" +
                                "        \"latitude\" : \"-1.0\"\n" +
                                "    },\n" +
                                "    \"connection_info\" : {\n" +
                                "        \"connection_   type\" : \"WIFI\"\n" +//tutaj
                                "    },\n" +
                                "    \"cpu\" : {\n" +
                                "        \"current\" : 0.0010613405611366034\n" +
                                "    },\n" +
                                "    \"last_updates\" : {\n" +
                                "        \"it\" : \"1421412449\"\n" +
                                "    },\n" +
                                "    \"storage\" : {\n" +
                                "        \"\\/storage\\/emulated\\/0\" : {\n" +
                                "            \"total\" : 28361302016,\n" +
                                "            \"free\" : 26784137216\n" +
                                "        }\n" +
                                "    }\n" +
                                "}"));
                String content = connectionMaker.Connect
                        ("http://app.peak-retail.com/web/api.php/event/heartbeat", keyValue);

                System.out.println(content);
                try {
                        ApiResponse apiResponse = new ResponseMapper().map(content, ApiResponse.class);
                        assertTrue("Status field incorrect", apiResponse.getStatus().equals("OK") ||
                                apiResponse.getStatus().equals("ERROR"));

                } catch (IOException e2) {
                        e2.printStackTrace();
                        fail("parsing error");
                }
        }

    //nie powinieneś nigdy dostać 500. Tylko 200, json lub info o błędzie. Skrypt który losowo usunie
    // kilka znaków.
    @Test//urządzenie z mojego biurka, dane logowania to przemek/przemek
    public void WrongApiKeyHeartbeatTest4() throws IOException {
            keyValue.add(new BasicNameValuePair("api_key", "8b8569gfjrghf9929"));
            keyValue.add(new BasicNameValuePair("registration_id",
                    "pQpIoVjVXvybpKuZ3PZxGko4PfCB2QEuM3Garg7ea8XUIXA8hHyGoEBRvB7CrxqIAI6HETx_00EOUxxsT9IX65bshP2CqHaVckL-8iTpOi4gz6naFQiEfl83TwzZ6g3ztM6O_31l33bpcqJyTcn2gCQ&"));
            keyValue.add(new BasicNameValuePair("data",
                    "{\"user_info\" : {\n" +
                            "        \"login\" : \"przemek\",\n" +
                            "        \"api_key\" : \"8b8569745eb411e49929\",\n" +
                            "        \"registration_id_md5\" : \"ae06549f44bd281d63d34d8968ff4c5e\"\n" +
                            "    },\n" +
                            "    \"device_uptime\" : {\n" +
                            "        \"uptime\" : \"454275\",\n" +
                            "        \"boot\" : \"1421401788\"\n" +
                            "    },\n" +
                            "    \"ram_info\" : {\n" +
                            "        \"total\" : \"44720128\",\n" +
                            "        \"free\" : \"27340192\"\n" +
                            "    },\n" +
                            "    \"app\" : {\n" +
                            "        \"android_version\" : \"4.2.2\",\n" +
                            "        \"version_name\" : \"1.14.07-K\",\n" +
                            "        \"model\" : \"OUTFORM\",\n" +
                            "        \"is_release\" : true,\n" +
                            "        \"app_running\" : true,\n" +
                            "        \"version_code\" : 83\n" +
                            "    },\n" +
                            "    \"location\" : {\n" +
                            "        \"longnitude\" : \"-1.0\",\n" +
                            "        \"latitude\" : \"-1.0\"\n" +
                            "    },\n" +
                            "    \"connection_info\" : {\n" +
                            "        \"connection_type\" : \"WIFI\"\n" +
                            "    },\n" +
                            "    \"cpu\" : {\n" +
                            "        \"current\" : 0.0010613405611366034\n" +
                            "    },\n" +
                            "    \"last_updates\" : {\n" +
                            "        \"it\" : \"1421412449\"\n" +
                            "    },\n" +
                            "    \"storage\" : {\n" +
                            "        \"\\/storage\\/emulated\\/0\" : {\n" +
                            "            \"total\" : 28361302016,\n" +
                            "            \"free\" : 26784137216\n" +
                            "        }\n" +
                            "    }\n" +
                            "}"));
            String content = connectionMaker.Connect
                    ("http://app.peak-retail.com/web/api.php/event/heartbeat", keyValue);

            System.out.println(content);
            try {
                    ApiResponse apiResponse = new ResponseMapper().map(content, ApiResponse.class);
                    assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR"));

            } catch (IOException e2) {
                    e2.printStackTrace();
                    fail("parsing error");
            }
    }

}
