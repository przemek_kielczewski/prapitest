package com.example.Done;


import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.annotation.HttpTest;
import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.LoginTest.Lang;
import com.example.JsonObjects.LoginTest.User;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by przemek on 12/15/14.
 */
public class LoginTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_USER = randomizer.makeRandomUser();
    String RANDOM_PASSWORD = randomizer.makeRandomUser();

    //preparations
    private boolean isLanguagePresent(List<Lang> langs, String langToCompare) {
        for (Lang lang : langs) {
            if (lang.getLang() != null && lang.getLang().equals(langToCompare)) {
                return true;
            }
        }
        return false;
    }

    //tests
    @Test
    public void loginCorrectTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "hortorus"));
        keyValue.add(new BasicNameValuePair("password", "hortorusqqq"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.

            User user = new ResponseMapper().map(content, User.class);

            assertTrue("Login Status should be OK", user.getStatus().equals("OK"));

            if (user != null) {
                assertNotNull("User code field is null", user.getCode());//fail int
                assertFalse("Api Key field is null", TextUtils.isEmpty(user.getApiKey()));
                assertFalse("lang field is null", TextUtils.isEmpty(user.getLang()));
                assertNotNull("langs collection is null", user.getLangs());
                assertTrue("langs collection doesn't contain user field lang",
                        isLanguagePresent(user.getLangs(), user.getLang()));//dopytaj Krzyśka
                assertFalse("Username field is null", TextUtils.isEmpty(user.getUsername()));
                assertNotNull("Retailer id field is null", user.getRetailerId());
                assertFalse("Retailer field is null", TextUtils.isEmpty(user.getRetailer()));
                assertFalse("Show Price field is null", TextUtils.isEmpty(user.getShowPrice()));
                assertFalse("Pin Code field is null", TextUtils.isEmpty(user.getPinCode()));
            } else {
                fail("User is null");
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }


    @Test
    public void loginIncorrectUsernameTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "horturs"));
        keyValue.add(new BasicNameValuePair("password", "hortorusqqq"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectPasswordTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "hortorus"));
        keyValue.add(new BasicNameValuePair("password", "hortrusqqq"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectEmptyTest4() throws IOException {
        keyValue.add(new BasicNameValuePair("username", ""));
        keyValue.add(new BasicNameValuePair("password", ""));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginExpiredLicenceTest5() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "testexpired"));
        keyValue.add(new BasicNameValuePair("password", "y7gfmhjf67"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1002"));
    }

   /* @HttpTest(test limitu licencji do zrobienia, na razie nie ma limitu, nie ma testu
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "username=testlimit1&password=fdghrt657hyrt"
    )
    public void loginLicenceLimitTest6() {
    String content = response.getBody();
        assertTrue("Diffrent Answer expected", content.contains("\"status\":\"ERROR\"")
                && content.contains("\"code\":\"1002\""));
        System.out.println(response.getBody());

    }*/

    @Test
    public void loginIncorrectRandomTest7() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "khudegsriw"));
        keyValue.add(new BasicNameValuePair("password", "jbafdekbfjewu"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest8() throws IOException {
        keyValue.add(new BasicNameValuePair("username", " "));
        keyValue.add(new BasicNameValuePair("password", " "));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest9() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "!@#"));
        keyValue.add(new BasicNameValuePair("password", "!@#"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest10() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "!!!!!!!!!!"));
        keyValue.add(new BasicNameValuePair("password", "!!!!!!!!!!"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest11() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "!@#$%^&*()-+"));
        keyValue.add(new BasicNameValuePair("password", "!@#$%^&*()-+"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest12() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "ęóąśłżźćń"));
        keyValue.add(new BasicNameValuePair("password", "ęóąśłżźćń"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }


    @Test
    public void loginIncorrectRandomTest13() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "!@#$%^&*()-+ęóąśłżźćń"));
        keyValue.add(new BasicNameValuePair("password", "!@#$%^&*()-+ęóąśłżźćń"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest14() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "1234567890"));
        keyValue.add(new BasicNameValuePair("password", "1234567890"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest15() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "123-123-123"));
        keyValue.add(new BasicNameValuePair("password", "123-123-123"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest16() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "666777999"));
        keyValue.add(new BasicNameValuePair("password", "666777999"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));

    }
   /* injections*/

    @Test
    public void loginIncorrectRandomTest17() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "="));
        keyValue.add(new BasicNameValuePair("password", ""));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest18() throws IOException {
        keyValue.add(new BasicNameValuePair("username", ""));
        keyValue.add(new BasicNameValuePair("password", "="));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));

    }

    @HttpTest(
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "username=&password=1=1"
    )
    public void loginIncorrectRandomTest19() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "1=1"));
        keyValue.add(new BasicNameValuePair("password", "1=1"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginIncorrectRandomTest20() throws IOException {
        keyValue.add(new BasicNameValuePair("username", "100; SELECT * FROM Retailers "));
        keyValue.add(new BasicNameValuePair("password", "1=1"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void loginRandomTest21() throws IOException {
        keyValue.add(new BasicNameValuePair("username", RANDOM_USER));
        keyValue.add(new BasicNameValuePair("password", RANDOM_PASSWORD));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/login", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }
}



