package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.RelatedProductsTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by przemek on 1/20/15.
 */
public class RelatedProductsTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcddlsgku11e4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";


    //tests
    @Test
    public void RelatedProductsTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/relatedProducts", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            RelatedProducts relatedProducts = new ResponseMapper().map(content, RelatedProducts.class);

            assertTrue("Status field incorrect", relatedProducts.getStatus().equals("OK")
                    || relatedProducts.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", relatedProducts.getItems());

            if (relatedProducts != null) {

                for (RelatedProductsItem item : relatedProducts.getItems()) {
                    assertNotNull("Id Field is null", item.getId());
                    assertTrue("Wrong relation id", item.getRelationId().equals(1) || item.getRelationId().equals(2));
                    assertNotNull("Parent Id Field is null", item.getParentId());
                    assertNotNull("Child Id Field is null", item.getChildId());
                }
                for (DeletedItem deletedItem : relatedProducts.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", relatedProducts.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", relatedProducts.getTime());
                assertTrue("Records field incorrect", relatedProducts.getItems().size() == relatedProducts.getRecords());
            } else {
                fail("Products is null");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void RelatedProductsTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/relatedProducts", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void RelatedProductsTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/relatedProducts", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
