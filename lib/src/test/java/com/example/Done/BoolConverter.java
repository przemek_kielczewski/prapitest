package com.example.Done;

import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * Created by przemek on 1/16/15.
 */
public class BoolConverter extends StdConverter <String, Boolean> {

    private static final String[] trueValues = {"true", "1"};


    @Override
    public Boolean convert(String value) {
        return trueValues[0].equals(value) || trueValues[1].equals(value);
    }
}
