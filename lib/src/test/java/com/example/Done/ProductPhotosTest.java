package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductPhotosTest.PhotoItem;
import com.example.JsonObjects.ProductPhotosTest.ProductPhotos;
import com.example.JsonObjects.ProductPhotosTest.DeletedItem;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.apache.http.util.TextUtils;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/12/15.
 */
public class ProductPhotosTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcslgnfljgje4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductPhotosTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/photos", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductPhotos productPhotos = new ResponseMapper().map(content, ProductPhotos.class);
            assertTrue("Status field incorrect", productPhotos.getStatus().equals("OK")
                    || productPhotos.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productPhotos.getItems());
//zrobione
            if (productPhotos != null) {

                for ( PhotoItem photoItem: productPhotos.getItems()) {
                //do omówienia
                    assertNotNull("id Field is null", photoItem.getId());
                    int id = photoItem.getId();
                    assertNotNull("Image field for id = " + id + " is null", photoItem.getImage());
                    assertFalse("Image field is empty", TextUtils.isEmpty(photoItem.getImage()));
                    assertNotNull("Product id is empty", photoItem.getProductId());
                    assertNotNull("real product id is empty",photoItem.getRealProductId());
                    assertNotNull("main field is empty", photoItem.getIsMain());
                    assertNotNull("retailer product id field is empty", photoItem.getRetailerProductId());
                }

                for (DeletedItem deletedItem: productPhotos.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }

                //zaakceptowane
                assertNotNull("zDuration field is null", productPhotos.getzDuration());
                //czy nie jest strasznie duże to duration, ale to potem
                assertNotNull("TimeStamp field is null", productPhotos.getTime());
                assertTrue("Records field incorrect", productPhotos.getItems().size() == productPhotos.getRecords());
            } else {
                fail("Products is null");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductPhotosTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/photos", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ProductPhotosTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/photos", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
