package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductsBarcodesTest.BarcodeItem;
import com.example.JsonObjects.ProductsBarcodesTest.ProductBarcodes;
import com.example.JsonObjects.ProductsBarcodesTest.DeletedItem;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/7/15.
 */
public class ProductBarcodesTest {
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd1dkljsbhfukigh4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";


    //tests
    @Test
    public void ProductBarcodesTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/barcodes", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductBarcodes productBarcodes = new ResponseMapper().map(content, ProductBarcodes.class);

            assertTrue("Status field incorrect", productBarcodes.getStatus().equals("OK")
                    || productBarcodes.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productBarcodes.getItems());


            if (productBarcodes != null) {
//zaakpcetowane
                for ( BarcodeItem barcodeItem : productBarcodes.getItems()) {
                assertNotNull("Id field is null", barcodeItem.getId());
                assertNotNull("Barcode field is null", barcodeItem.getBarcode());
                assertNotNull("Pid field is null", barcodeItem.getPid());
                assertNotNull("Real Product Id field is null", barcodeItem.getRealProductId());
                assertNotNull("Retailer Product Id field is null", barcodeItem.getRetailerProductId());
            }

                for (DeletedItem deletedItem: productBarcodes.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
//zaakceptowane//
                assertNotNull("zDuration field is null", productBarcodes.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", productBarcodes.getTime());
                assertTrue("Records field incorrect",
                        productBarcodes.getItems().size() == productBarcodes.getRecords());
            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductBarcodesTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/barcodes", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ProductBarcodesTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/barcodes", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
