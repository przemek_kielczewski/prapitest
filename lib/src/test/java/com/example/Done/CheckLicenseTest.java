package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.CheckLicenseTest.CheckLicense;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class CheckLicenseTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    public String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd12037dfghgdh2e";//incorrect
    public static final String ERROR_CODE = "1003";

    //tests
    @Test
    public void CheckLicenseTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/user/checkLicense", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            CheckLicense checkLicense = new ResponseMapper().map(content, CheckLicense.class);

            assertNotNull("Expiration TimeStamp field is null", checkLicense.getExpiriationTs());
            assertNotNull("Expiration Date field is null", checkLicense.getExpiriationDate());
            assertNotNull("Still valid field is true", checkLicense.getStillValid());

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }
    }

    @Test
    public void CheckLicenseTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/user/checkLicense", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void CheckLicenseTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/user/checkLicense", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}

