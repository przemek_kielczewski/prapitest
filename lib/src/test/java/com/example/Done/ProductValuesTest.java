package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductValuesTest.DeletedItem;
import com.example.JsonObjects.ProductValuesTest.ProductValues;
import com.example.JsonObjects.ProductValuesTest.ValuesItem;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by przemek on 1/14/15.
 */
public class ProductValuesTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1lljodhd37411e4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductValuesTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/productValues", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductValues productValues =
                    new ResponseMapper().map(content, ProductValues.class);
            assertTrue("Status field incorrect", productValues.getStatus().equals("OK")
                    || productValues.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productValues.getItems());

            if (productValues != null) {

                for ( ValuesItem valuesItem: productValues.getItems()) {
                    //do omówienia
                    assertNotNull("id Field is null", valuesItem.getId());
                    assertNotNull("Retailer id Field is null", valuesItem.getRetailerProductId());
                    assertNotNull("Attribute id Field is null", valuesItem.getAttributeId());
                }

                for (DeletedItem deletedItem: productValues.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }

                //zaakceptowane
                assertNotNull("zDuration field is null", productValues.getzDuration());
                //czy nie jest strasznie duże to duration, ale to potem
                assertNotNull("TimeStamp field is null", productValues.getTime());
                assertTrue("Records field incorrect", productValues.getItems().size() == productValues.getRecords());
            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductValuesTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/productValues", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ProductValuesTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/productValues", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

}
