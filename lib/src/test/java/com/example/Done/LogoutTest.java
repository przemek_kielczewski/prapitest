package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

/**
 * Created by przemek on 1/20/15.
 */
public class LogoutTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_USERNAME = randomizer.makeRandomUser();
    public static final String USERNAME = "hortorus";
    public static final String INCORRECT_USERNAME = "hotrus";
    public static final String ERROR_CODE = "1003";

    //tests
    @Test
    public void CorrectLogoutTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("username", USERNAME));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/logout", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isOk());
    }

    @Test
    public void IncorrectLogoutTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("username", INCORRECT_USERNAME));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/logout", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void RandomLogoutTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("username", RANDOM_USERNAME));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/logout", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
