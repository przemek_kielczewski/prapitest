package com.example.Done;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.assertTrue;


/**
 * Created by przemek on 1/20/15.
 */

public class SqLiteTest {

    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";//Przemek
    public static final String TIMESTAMP = "1";

    private int bufferSize = 1024;
    int read;
    int done = 0;
    File gzipDatabaseFile = new File("SpakowanyPlik");

    @Test
    public void SQLiteTest1() {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://app.peak-retail.com/web/api_dev.php/product/sqlite");
        List<NameValuePair> keyValue = new ArrayList<NameValuePair>();
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(keyValue));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            InputStream in = entity.getContent();
            OutputStream out = new FileOutputStream(gzipDatabaseFile);
            int total = (int) entity.getContentLength();
            BufferedInputStream bis = new BufferedInputStream(in);
            BufferedOutputStream bos = new BufferedOutputStream(out);
            byte[] buffer = new byte[bufferSize];
            while ((read = in.read(buffer, 0, bufferSize)) != -1) {
                bos.write(buffer, 0, read);
                done += read;
            }
            bos.flush();
            bos.close();
            bis.close();
            in.close();
            System.out.println(gzipDatabaseFile.length());
            assertTrue("ZippedDatabase is empty", gzipDatabaseFile.length() > 1);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            File dbOutputFile = new File("BazaDanych");
            this.gunzip(gzipDatabaseFile, dbOutputFile);
            int success = gunzip(gzipDatabaseFile, dbOutputFile);
            if (success > 0) {
                System.out.println(dbOutputFile.length());
                assertTrue("UnzippedDatabase Too small", dbOutputFile.length() > gzipDatabaseFile.length());
            }
            this.connectionAndVerify();
            }
        }

        //1. zapisz pod jakąś dowolną nazwą i czy 2. plik jest poprawny? ok
        //2. więcej  niż zero? ok
        //2. rozpakować, po rozpakowaniu powinien się nazywać jak apikey. Nadaję nazwę sam, ale ok.
        //3. Dodatkowo potem Połącz się z nim prze javę, żeby sprawdzić czy to jest sqlite i czy zawiera
        // odpowiednie pola? ok


    public int gunzip(File gzipDatabaseFile, File dbOutputFile) {
        int done = 0;
        try {
            int total = (int) gzipDatabaseFile.length();
            GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(gzipDatabaseFile));
            OutputStream out = new FileOutputStream(dbOutputFile);
            byte[] buffer = new byte[bufferSize];
            int read = -1;
            while ((read = gzis.read(buffer, 0, bufferSize)) != -1) {
                out.write(buffer, 0, read);
                done += read;
            }
            out.flush();
            out.close();
            gzis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return done;
    }

    public void connectionAndVerify() {
        List<String> tables = new ArrayList<String>();

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:BazaDanych");
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT name FROM sqlite_master WHERE type='table'");
            while (result.next()){
                tables.add(result.getString("name"));
                System.out.println(result.getString("name"));//do usunięcia
            }
            assertTrue("table product is missing", tables.contains("product"));
            assertTrue("table value is missing", tables.contains("value"));
            assertTrue("table attribute is missing", tables.contains("attribute"));
            assertTrue("table product_values is missing", tables.contains("product_values"));
            assertTrue("table product_photo is missing", tables.contains("product_photo"));
            assertTrue("table plant_combinations is missing", tables.contains("plant_combinations"));
            assertTrue("table related_product is missing", tables.contains("related_product"));
            assertTrue("table inspiration is missing", tables.contains("inspiration"));
            assertTrue("table sections is missing", tables.contains("sections"));

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
