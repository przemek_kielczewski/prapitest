package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductScreenSaverTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class ScreenSaverTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Przemek
    public static final String INCORRECT_API_KEY = "7c1dcd1kahfie4ab2e";
    public static final String TIMESTAMP = "293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ScreenSaverTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/Screensaver", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductScreenSaver productScreenSaver = new ResponseMapper().map(content, ProductScreenSaver.class);

            assertTrue("Status field incorrect", productScreenSaver.getStatus().equals("OK")
                    || productScreenSaver.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", productScreenSaver.getItems());

            if (productScreenSaver != null) {

                for (ProductScreenSaverItem item : productScreenSaver.getItems()) {
                    assertNotNull("Id field is null", item.getId());
                    assertNotNull("RetailerId field is null", item.getRetailerId());
                    assertNotNull("Position field is null", item.getPosition());
                    assertNotNull("Image field is null", item.getImage());
                    assertNotNull("Delay field is null", item.getDelay());
                    assertTrue("AnimationIn field incorrect", item.getAnimationIn().equals("NONE") ||
                            item.getAnimationIn().equals("FADE_IN") || item.getAnimationIn().equals("FADE_IN_BLACK")
                    || item.getAnimationIn().equals("SLIDE_IN_RIGHT") || item.getAnimationIn().equals("SLIDE_IN_LEFT")
                            || item.getAnimationIn().equals("SLIDE_IN_TOP") || item.getAnimationIn().equals("SLIDE_IN_BOTTOM"));

                    assertTrue("AnimationOut field incorrect", item.getAnimationOut().equals("NONE") ||
                            item.getAnimationOut().equals("FADE_OUT") || item.getAnimationOut().equals("FADE_OUT_BLACK")
                    || item.getAnimationOut().equals("SLIDE_OUT_RIGHT") || item.getAnimationOut().equals("SLIDE_OUT_LEFT")
                            || item.getAnimationOut().equals("SLIDE_OUT_TOP") || item.getAnimationOut().equals("SLIDE_OUT_BOTTOM"));

                }
                for (DeletedItem deletedItem : productScreenSaver.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", productScreenSaver.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", productScreenSaver.getTime());
                assertTrue("Records field incorrect", productScreenSaver.getItems().size() == productScreenSaver.getRecords());

            } else {
                fail("Products is null");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ScreenSaverTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/Screensaver", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ScreenSaverTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/Screensaver", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
