package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.AppScreenListTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;


import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class AppScreenListTest {
    //preparations
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    public String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";//hortorus
    public static final String INCORRECT_API_KEY = "8b8569ljdfsgjdgf49929";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void AppScreenListTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/appscreen/screenList", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            AppScreenMain appScreenMain = new ResponseMapper().map (content, AppScreenMain.class);

            if (appScreenMain != null) {
                assertNotNull("retailer_id field is null", appScreenMain.getRetailerId());
            }

            for (AppScreenItem appScreenItem : appScreenMain.getScreenItem()) {
                assertNotNull("Screen id field is null", appScreenItem.getId());
                assertTrue("Screen Home field is false", appScreenItem.getHome());

                for (ScreenMedia screenMedia : appScreenItem.getScreenMedia()) {
                    if (screenMedia != null) {
                        assertNotNull("screenMedia id field is null", screenMedia.getId());
                    }
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void AppScreenListTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/appscreen/screenList", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void AppScreenListTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/appscreen/screenList", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}

