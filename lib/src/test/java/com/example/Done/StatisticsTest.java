package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.StatisticsTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class StatisticsTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_REGISTRATION_ID = randomizer.makeRandomRegistrationid();
    String RANDOM_DATE_FROM = randomizer.makeRandomPastTimeStamp();
    String RANDOM_DATE_TO = randomizer.makeRandomRecentTimeStamp();

    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";//Przemek
    public static final String INCORRECT_API_KEY = "8b856974rgreththte9929";
    public static final String REGISTRATION_ID = "4567";
    public static final String DATE_FROM = "1293793200";
    public static final String DATE_TO = "1422004740";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void StatisticsTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("registration_id", REGISTRATION_ID));
        keyValue.add(new BasicNameValuePair("date_from", DATE_FROM));
        keyValue.add(new BasicNameValuePair("date_to", DATE_TO));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/stat/index", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Statistics statistics = new ResponseMapper().map(content, Statistics.class);

            assertTrue("Status field incorrect", statistics.getStatus().equals("OK")
                    || statistics.getStatus().equals("ERROR"));
            assertNotNull("Totals Collection is null", statistics.getTotals());
            assertNotNull("Retailer Statistics Collection is null", statistics.getRetailerStatistics());
            assertNotNull("Device Statistics is null", statistics.getDeviceStatistics());

            if (statistics != null) {// Czy to zostaje kiedy korzystam z Response Mappera?
                Totals totals = statistics.getTotals();

                    assertNotNull("search By Phrase field is null", totals.getSearchesByPhrase());
                    assertNotNull("search By Attribute field is null", totals.getSearchesByAttribute());
                    assertNotNull("search By userSessions field is null",totals.getUserSessions());
                    assertNotNull("search by productViews",totals.getProductViews());
                    //totals

                RetailerStatistics retailerStatistics = statistics.getRetailerStatistics();
                assertNotNull("search By Phrase field is null", retailerStatistics.getRetailerPhrase());
                assertNotNull("search By Attribute field is null", retailerStatistics.getRetailerAttribute());
                assertNotNull("product Views field is null", retailerStatistics.getRetailerView());
                assertNotNull("product Scans is null", retailerStatistics.getRetailerScan());
                assertNotNull("Active hours is null", retailerStatistics.getRetailerHours());

                DeviceStatistics deviceStatistics = statistics.getDeviceStatistics();
                assertNotNull("search By Phrase field is null", deviceStatistics.getRetailerPhrase());
                assertNotNull("search By Attribute field is null", deviceStatistics.getRetailerAttribute());
                assertNotNull("product Views field is null", deviceStatistics.getRetailerView());
                assertNotNull("product Scans is null", deviceStatistics.getRetailerScan());
                assertNotNull("Active hours is null", deviceStatistics.getRetailerHours());

            } else {
                fail("Products is null");
            }

            assertNotNull("zDuration field is null", statistics.getzDuration());
            //czy nie jest strasznie duże to duration ale to potem
            assertNotNull("TimeStamp field is null", statistics.getTime());

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }


    }

    @Test
    public void StatisticsTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("registration_id", REGISTRATION_ID));
        keyValue.add(new BasicNameValuePair("date_from", DATE_FROM));
        keyValue.add(new BasicNameValuePair("date_to", DATE_TO));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/stat/index", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void StatisticsTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("registration_id", RANDOM_REGISTRATION_ID));
        keyValue.add(new BasicNameValuePair("date_from", RANDOM_DATE_FROM));
        keyValue.add(new BasicNameValuePair("date_to", RANDOM_DATE_TO));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/stat/index", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

}
