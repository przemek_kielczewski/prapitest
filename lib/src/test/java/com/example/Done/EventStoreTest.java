package com.example.Done;

import com.eclipsesource.restfuse.*;
import com.eclipsesource.restfuse.annotation.HttpTest;
import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

/**
 * Created by przemek on 1/20/15.
 */
public class EventStoreTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();


    //tests

    @Test//urządzenie z mojego biurka, dane logowania to przemek/przemek
    public void CorrectEventStoreTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", "8b8569745eb411e49929"));
        keyValue.add(new BasicNameValuePair("registration_id",
                "pQpIoVjVXvybpKuZ3PZxGko4PfCB2QEuM3Garg7ea8XUIXA8hHyGoEBRvB7CrxqIAI6HETx_00EOUxxsT9IX65bshP2CqHaVckL-8iTpOi4gz6naFQiEfl83TwzZ6g3ztM6O_31l33bpcqJyTcn2gCQ"));
        keyValue.add(new BasicNameValuePair("login", "przemek"));
        keyValue.add(new BasicNameValuePair("version", "1.14.07-K"));
        keyValue.add(new BasicNameValuePair("release", "true"));
        keyValue.add(new BasicNameValuePair("event", "scan"));
        keyValue.add(new BasicNameValuePair("param", "6"));
        keyValue.add(new BasicNameValuePair("time", "1"));
        keyValue.add(new BasicNameValuePair("lang", "null"));
        keyValue.add(new BasicNameValuePair("attribute_id", "null"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/event/store", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isOk());
    }


    @Test//urządzenie z mojego biurka, dane logowania to przemek/przemek
    public void TypoEventStoreTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("a_key", "8b8569745eb411e49929"));
        keyValue.add(new BasicNameValuePair("regist_id", "1234"));
        keyValue.add(new BasicNameValuePair("login", "przemek"));
        keyValue.add(new BasicNameValuePair("veldsjn", "1.14.07-K"));
        keyValue.add(new BasicNameValuePair("release", "true"));
        keyValue.add(new BasicNameValuePair("event", "scan"));
        keyValue.add(new BasicNameValuePair("pram", "6"));
        keyValue.add(new BasicNameValuePair("time", "1"));
        keyValue.add(new BasicNameValuePair("lang", "null"));
        keyValue.add(new BasicNameValuePair("attribute_id", "null"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/event/store", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("2003"));
    }


    @Test
    public void TypoEventStoreTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", "8b8569745eb411e49929"));
        keyValue.add(new BasicNameValuePair("registration_id", "1234"));
        keyValue.add(new BasicNameValuePair("login", "przenek"));
        keyValue.add(new BasicNameValuePair("version", "1.14.07-K"));
        keyValue.add(new BasicNameValuePair("release", "tue"));
        keyValue.add(new BasicNameValuePair("event", "scn"));
        keyValue.add(new BasicNameValuePair("param", "6"));
        keyValue.add(new BasicNameValuePair("time", "1"));
        keyValue.add(new BasicNameValuePair("lang", "nu"));
        keyValue.add(new BasicNameValuePair("attribute_id", "nul"));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/event/store", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("2013"));
        }

/*
    @HttpTest(//urządzenie z mojego biurka, dane logowania to przemek/przemek
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "api_key=8b8569745eb411e49929&registration_id=1234&version=1.14.07-K&" +
                    "release=true&event=scan&param=6&time=1&lang=null&attribute_id=null"
    )
    public void MissingloginEventStoreTest4() {
        ObjectMapper objectMapper = initJsonMapper();
        String content = response.getBody();
        System.out.println(content);
        try {
            ApiResponse apiResponse = objectMapper.readValue(content, ApiResponse.class);
            assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR")
                    && apiResponse.getMessage().equals("login missing"));
        } catch (IOException e2) {
            e2.printStackTrace();
            fail("parsing error");
        }
    }


    @HttpTest(//urządzenie z mojego biurka, dane logowania to przemek/przemek
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "api_key=8b8569745eb411e49929&registration_id=1234&login=przemek&" +
                    "release=true&event=scan&param=6&time=1&lang=null&attribute_id=null"
    )
    public void MissingVersionEventStoreTest5() {
        ObjectMapper objectMapper = initJsonMapper();
        String content = response.getBody();
        System.out.println(content);
        try {
            ApiResponse apiResponse = objectMapper.readValue(content, ApiResponse.class);
            assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR")
                    && apiResponse.getMessage().equals("version missing"));
        } catch (IOException e2) {
            e2.printStackTrace();
            fail("parsing error");
        }
    }

    @HttpTest(//urządzenie z mojego biurka, dane logowania to przemek/przemek
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "api_key=8b8569745eb411e49929&registration_id=1234&login=przemek&version=1.14.07-K&" +
                    "release=true&param=6&time=1&lang=null&attribute_id=null"
    )
    public void MissingEventEventStoreTest6() {
        ObjectMapper objectMapper = initJsonMapper();
        String content = response.getBody();
        System.out.println(content);
        try {
            ApiResponse apiResponse = objectMapper.readValue(content, ApiResponse.class);
            assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR")
                    && apiResponse.getMessage().equals("event missing"));
        } catch (IOException e2) {
            e2.printStackTrace();
            fail("parsing error");
        }
    }

    @HttpTest(//urządzenie z mojego biurka, dane logowania to przemek/przemek
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "api_key=8b8569745eb411e49929&registration_id=1234&login=przemek&version=1.14.07-K&" +
                    "release=true&event=scan&time=1&lang=null&attribute_id=null"
    )
    public void MissingParamEventStoreTest7() {
        ObjectMapper objectMapper = initJsonMapper();
        String content = response.getBody();
        System.out.println(content);
        try {
            ApiResponse apiResponse = objectMapper.readValue(content, ApiResponse.class);
            assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR")
                    && apiResponse.getMessage().equals("param missing"));
        } catch (IOException e2) {
            e2.printStackTrace();
            fail("parsing error");
        }
    }

    @HttpTest(//urządzenie z mojego biurka, dane logowania to przemek/przemek
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "api_key=8b8569745eb411e49929&registration_id=1234&login=przemek&version=1.14.07-K&" +
                    "release=true&event=scan&param=6&lang=null&attribute_id=null"
    )
    public void MissingTimeEventStoreTest8() {
        ObjectMapper objectMapper = initJsonMapper();
        String content = response.getBody();
        System.out.println(content);
        try {
            ApiResponse apiResponse = objectMapper.readValue(content, ApiResponse.class);
            assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR")
                    && apiResponse.getMessage().equals("time missing"));
        } catch (IOException e2) {
            e2.printStackTrace();
            fail("parsing error");
        }
    }

    @HttpTest(//urządzenie z mojego biurka, dane logowania to przemek/przemek
            method = Method.POST,
            path = "/",
            type = MediaType.APPLICATION_FORM_URLENCODED,
            content = "api_key=8b8569745eljdfsghkg9929&registration_id=1234&login=przemek&version=1.14.07-K&" +
                    "release=true&event=scan&time=1&param=6&lang=null&attribute_id=null"
    )
    public void WrongApiKeyEventStoreTest9() {
        ObjectMapper objectMapper = initJsonMapper();
        String content = response.getBody();
        System.out.println(content);
        try {
            ApiResponse apiResponse = objectMapper.readValue(content, ApiResponse.class);
            assertTrue("Status field incorrect", apiResponse.getStatus().equals("ERROR"));
        } catch (IOException e2) {
            e2.printStackTrace();
            fail("parsing error");
        }
    }*/
}
