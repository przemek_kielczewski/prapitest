package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductVariationsTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/23/15.
 */
public class ProductVariationsTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "1f512b4b698811e3ab2e";//tylko tu są variations
    public static final String INCORRECT_API_KEY = "1f512b4bdfgfhj2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void VariationsTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/variations", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Variations variations = new ResponseMapper().map(content, Variations.class);

            assertTrue("Status field incorrect", variations.getStatus().equals("OK")
                    || variations.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", variations.getItems());

            if (variations != null) {

                for (VariationsItem item : variations.getItems()) {
                    assertNotNull("id field is null", item.getId());
                    assertNotNull("retailer product id field is null", item.getRetailerProductId());

                    for (Attributes attributes : item.getAttributeItems()) {
                        assertNotNull("attribute id field is null", attributes.getAttributeId());
                        assertFalse("textvalue field is null", TextUtils.isEmpty(attributes.getTextValue()));
                    }
                    assertFalse("type field is null", TextUtils.isEmpty(item.getType()));
                }
                for (DeletedItem deletedItem : variations.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", variations.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", variations.getTime());
                assertTrue("Records field incorrect", variations.getItems().size() == variations.getRecords());

            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void VariationsTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/variations", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void VariationsTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/variations", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
