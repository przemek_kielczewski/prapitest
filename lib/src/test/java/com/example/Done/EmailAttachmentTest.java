package com.example.Done;


import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */

public class EmailAttachmentTest {
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    public String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    public String RANDOM_AGREEMENT = randomizer.makeRandomAgreement();
    public String RANDOM_MID = randomizer.makeRandomAgreement();


    public static final String CORRECT_API_KEY = "blum2A7W89RTxyz";//Blumea, tylko tutaj działa attachment
    public static final String INCORRECT_API_KEY = "bludgjryjy89RTxyz";
    public static final String EMAIL = "przemek.kielczewski@moveapp.pl";
    public static final String AGREEMENT = "0";
    public static final String MID = "4";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductEmailAttachmentTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("email", EMAIL));
        keyValue.add(new BasicNameValuePair("agreement", AGREEMENT));
        keyValue.add(new BasicNameValuePair("mid", MID));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/email/emailattachment", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Diffrent answer expected", apiResponse.isOk());
    }

    @Test
    public void ProductEmailAttachmentTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("email", EMAIL));
        keyValue.add(new BasicNameValuePair("agreement", AGREEMENT));
        keyValue.add(new BasicNameValuePair("mid", MID));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/email/emailattachment", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ProductEmailAttachmentTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("email", EMAIL));
        keyValue.add(new BasicNameValuePair("agreement", RANDOM_AGREEMENT));
        keyValue.add(new BasicNameValuePair("mid", RANDOM_MID));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/email/emailattachment", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
