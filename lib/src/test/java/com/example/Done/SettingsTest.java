package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.SetttingsTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.TextUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/20/15.
 */
public class SettingsTest {
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_USER = randomizer.makeRandomUser();
    String RANDOM_REGISTRATION_ID = randomizer.makeRandomRegistrationid();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd12dsljohue4ab2e";
    public static final String REGISTRATION_ID = "657856";
    public static final String USERNAME = "hortorus";
    public static final String ERROR_CODE = "2003";

    private boolean isLanguagePresent(List<Lang> langs, String langToCompare) {
        for (Lang lang : langs) {
            if (lang.getLang() != null && lang.getLang().equals(langToCompare)) {
                return true;
            }
        }
        return false;
    }

    //tests
    @Test
    public void SettingsCorrectTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("username", USERNAME));
        keyValue.add(new BasicNameValuePair("registration_id", REGISTRATION_ID));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/settings", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.

            assertTrue("Login Status should be OK", content.contains("\"status\":\"OK\""));

            Settings settings = new ResponseMapper().map(content, Settings.class);

            if (settings != null) {
                assertFalse("User code field is null", settings.getCode() == null);//fail int
                assertFalse("Api Key field is null", TextUtils.isEmpty(settings.getApiKey()));
                assertFalse("lang field is null", TextUtils.isEmpty(settings.getLang()));
                assertFalse("langs collection is null", settings.getLangs() == null);
                assertTrue("langs collection doesn't contain user field lang",
                        isLanguagePresent(settings.getLangs(), settings.getLang()));//dopytaj Krzyśka
                assertFalse("Username field is null", TextUtils.isEmpty(settings.getUsername()));
                assertFalse("Retailer id field is null", settings.getRetailerId() == null);
                assertFalse("Retailer field is null", TextUtils.isEmpty(settings.getRetailer()));
                assertFalse("Show Price field is null", TextUtils.isEmpty(settings.getShowPrice()));
                assertFalse("Pin Code field is null", TextUtils.isEmpty(settings.getPinCode()));
            } else {
                fail("User is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void SettingsWrongTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("username", USERNAME));
        keyValue.add(new BasicNameValuePair("registration_id", REGISTRATION_ID));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/settings", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

    @Test
    public void SettingsRandomTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("username", RANDOM_USER));
        keyValue.add(new BasicNameValuePair("registration_id", RANDOM_REGISTRATION_ID));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/user/settings", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode("1003"));
    }

}
