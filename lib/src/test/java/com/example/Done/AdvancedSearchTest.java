package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.AdvancedSearchTest.*;
import com.example.JsonObjects.ApiResponse;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;


/**
 * Created by przemek on 1/20/15.
 */

public class AdvancedSearchTest {
    //preparations
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    public String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    public String RANDOM_TIME_STAMP = randomizer.makeRandomRecentTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd1203klj1e4ab2e";//incorrect
    public static final String timestamp = "1293793200";//12/31/2010
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void AdvancedSearchTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", timestamp));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/advancedSearch", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            AdvancedSearch advancedSearch = new ResponseMapper().map(content, AdvancedSearch.class);

            assertTrue("Status field incorrect", advancedSearch.getStatus().equals("OK"));
            assertNotNull("Products Collection is null", advancedSearch.getItems());

            if (advancedSearch != null) {

                for (AdvancedSearchItem advancedSearchItem : advancedSearch.getItems()) {
                    assertNotNull("id field is null", advancedSearchItem.getId());
                    assertNotNull("retailerId field is null", advancedSearchItem.getRetailerId());
                    assertNotNull("Name field is null", advancedSearchItem.getName());
                    assertNotNull("Elements List is null", advancedSearchItem.getList());

                    for (AdvancedSearchElement advancedSearchElement : advancedSearchItem.getList()) {
                        assertNotNull("Element id field is null", advancedSearchElement.getId());
                        assertNotNull("Element SectionId field is null", advancedSearchElement.getSectionId());
                        assertNotNull("Element attributeId field is null", advancedSearchElement.getAttributeId());
                        assertNotNull("Element Position field is null", advancedSearchElement.getPosition());
                    }

                }
                for (DeletedItem deletedItem : advancedSearch.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", advancedSearch.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", advancedSearch.getTime());
                assertTrue("Records field incorrect", advancedSearch.getItems().size() == advancedSearch.getRecords());

            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    //7c1dcd1203klj;1e4ab2e - invalid params format
    public void AdvancedSearchTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", timestamp));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/advancedSearch", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    //7c1dcd1203klj;1e4ab2e - invalid params format
    public void AdvancedSearchTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIME_STAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/advancedSearch", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}


