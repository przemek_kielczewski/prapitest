package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductsTest.DeletedItem;
import com.example.JsonObjects.ProductsTest.ProductsItem;
import com.example.JsonObjects.ProductsTest.Products;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;
/**
 * Created by przemek on 12/31/14.
 */
public class ProductsTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();
    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcsdfskhyu2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String TOTALTIME = "1";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductsWithoutTotalTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/products", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Products products = new ResponseMapper().map(content, Products.class);

            assertTrue("Status field incorrect", products.getStatus().equals("OK")
                    || products.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", products.getItems());

            if (products != null) {

                for ( ProductsItem item: products.getItems()) {
                    assertNotNull("id field is null", item.getId()); //obowiązkowe
                    assertNotNull("product id field is null", item.getProductId()); //obowiązkowe
                    assertNotNull("retailer id is null", item.getRetailerId()); //obowiązkowe
                    assertNotNull("external id is null", item.getExternalId()); //external_id może być pusty
                    assertNotNull("HideInheritedImages is null", item.isHideInheritedImages());
                    //obowiązkowe true lub false
                    assertNotNull("active field is null", item.getActive());
                    assertTrue("active field error", item.getActive().equals("true") || item.getActive().equals("false"));
                    assertNotNull("OnlyForRelated is null", item.isOnlyForRelated()); //obowiązkowe true lub false
                    assertNotNull("complete field is null", item.isComplete()); //obowiązkowe true lub false
                    assertNotNull("Type field is empty", item.getType()); //obowiązkowe String
                }
                for (DeletedItem deletedItem: products.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", products.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", products.getTime());
                assertTrue("Records field incorrect", products.getItems().size() == products.getRecords());

            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

    @Test
    public void ProductsWithTotalTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        keyValue.add(new BasicNameValuePair("total", TOTALTIME));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/products", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Products products = new ResponseMapper().map(content, Products.class);

            if (products != null) {
                assertTrue("Status field incorrect", products.getStatus().equals("OK")
                        || products.getStatus().equals("ERROR"));
                assertNotNull("Products Collection is null", products.getItems());

                for ( ProductsItem item: products.getItems()) {
                    assertNotNull("id field is null", item.getId()); //obowiązkowe
                    assertNotNull("product id field is null", item.getProductId()); //obowiązkowe
                    assertNotNull("retailer id is null", item.getRetailerId()); //obowiązkowe
                    assertNotNull("external id is null", item.getExternalId()); //external_id może być pusty
                    assertNotNull("HideInheritedImages is null", item.isHideInheritedImages());
                    //obowiązkowe true lub false
                    assertNotNull("active field is null", item.getActive());
                    assertTrue("active field error", item.getActive().equals("true") || item.getActive().equals("false"));
                    assertNotNull("OnlyForRelated is null", item.isOnlyForRelated()); //obowiązkowe true lub false
                    assertNotNull("complete field is null", item.isComplete()); //obowiązkowe true lub false
                    assertNotNull("Type field is empty", item.getType()); //obowiązkowe String
                }
                for (DeletedItem deletedItem: products.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }
                assertNotNull("zDuration field is null", products.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", products.getTime());
                assertTrue("Records field incorrect", products.getItems().size() == products.getRecords());

            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void ProductsTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/products", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void ProductsTest4() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/products", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }
}
