package com.example.Done;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.ProductTypesTest.ProductTypes;
import com.example.JsonObjects.ProductTypesTest.TypeItem;
import com.example.JsonObjects.ProductTypesTest.DeletedItem;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 12/31/14.
 */
public class ProductTypesTest {

    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    String RANDOM_TIMESTAMP = randomizer.makeRandomFutureTimeStamp();

    public static final String CORRECT_API_KEY = "7c1dcd12037411e4ab2e";//Hortorus
    public static final String INCORRECT_API_KEY = "7c1dcd1dkljsbhfukigh4ab2e";
    public static final String TIMESTAMP = "1293793200";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void ProductTypesTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/productTypes", keyValue);

        /*myContent = "{\"status\":\"OK\",\"items\":[{\"id\":\"1\",\"default\":\"0\"," +
                "\"is_default\":\"0\",\"name\":\"Rośliny\"," +
                "\"hide_inherited_images\":\"0\",\"active\":\"true\",\"only_for_related\":0," +
                "\"complete\":false,\"type\":\"Ro\\u015bliny\"}],\"deleted\":[{\"id\":\"1\"}]," +
                "\"total\":\"100\",\"z_duration\":4.0955588817596,\"time\":1420715263,\"records\":1}";*/
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            ProductTypes productTypes = new ResponseMapper().map(content, ProductTypes.class);

            assertTrue("Status field incorrect", productTypes.getStatus().equals("OK" )
                    || productTypes.getStatus().equals("ERROR" ));
            assertNotNull("Products Collection is null", productTypes.getItems());

            if (productTypes != null) {

                for (TypeItem typeItem : productTypes.getItems()) {
                    assertNotNull("id field is null", typeItem.getId());
                    assertNotNull("DefaultType field is null", typeItem.isDefaultType()); //conflicting types coś się pluje
                    assertNotNull("Default field is null", typeItem.isDefaultValue());
                    assertNotNull("Type name field is null", typeItem.getName());
                    assertNotNull("Attributes List is null", typeItem.getAttributesList());
                }
                //attributesList

                for (DeletedItem deletedItem : productTypes.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());
                }

                //zaakceptowane
                assertNotNull("zDuration field is null", productTypes.getzDuration());
                //czy nie jest strasznie duże to duration, ale to potem
                assertNotNull("TimeStamp field is null", productTypes.getTime());
                assertTrue("Records field incorrect", productTypes.getItems().size() == productTypes.getRecords());
            } else {
                fail("Products is null" );
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error" );
        }

    }

    @Test
    public void ProductTypesTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/productTypes", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE ));
    }

    @Test
    public void ProductTypesTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", RANDOM_TIMESTAMP));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api_dev.php/product/productTypes", keyValue);
        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Different answer expected", apiResponse.isErrorWithCode(ERROR_CODE ));
    }
}
