package com.example.Done;


import com.example.ConnectionMaker;
import com.example.JsonObjects.ApiResponse;
import com.example.JsonObjects.AppScreenTest.*;
import com.example.Randomizer;
import com.example.mapper.ApiResponseMapper;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by przemek on 1/22/15.
 */

public class AppScreenTest {
    Randomizer randomizer = new Randomizer();
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    public String RANDOM_API_KEY = randomizer.makeRandomApiKey();
    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";//Przemek
    public static final String INCORRECT_API_KEY = "8b856974sfdjbnkhs29";//incorrect
    public static final String SCREEN_ID = "73";
    public static final String ERROR_CODE = "2003";

    //tests
    @Test
    public void AppScreenTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("id", SCREEN_ID));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/appscreen/screen", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            AppScreenItem appScreenItem = new ResponseMapper().map (content, AppScreenItem.class);

            if (appScreenItem != null) {
                assertNotNull("Id field is null", appScreenItem.getId());
                assertTrue("Home field is false", appScreenItem.getHome());
            }

            for (ScreenMedia element : appScreenItem.getScreenMedia()) {

                if (element != null) {
                    assertNotNull("Media Id field is null", element.getId());
                }
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }

    }

    @Test
    public void AppScreenTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", INCORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("id", SCREEN_ID));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/appscreen/screen", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

    @Test
    public void AppScreenTest3() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", RANDOM_API_KEY));
        keyValue.add(new BasicNameValuePair("id", SCREEN_ID));
        System.out.println(keyValue.toString());
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/appscreen/screen", keyValue);

        ApiResponse apiResponse = new ApiResponseMapper().map(content);
        System.out.println(content);
        assertTrue("Diffrent answer expected", apiResponse.isErrorWithCode(ERROR_CODE));
    }

}
