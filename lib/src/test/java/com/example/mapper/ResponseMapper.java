package com.example.mapper;

import com.example.JsonObjects.ApiResponse;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by przemek on 2/4/15.
 */
public class ResponseMapper {

    private ObjectMapper objectMapper;

    public <T> T map(String content, Class<T> valueType) throws IOException {
        if (objectMapper == null) {
            initJsonMapper();
        }
        return objectMapper.readValue(content, valueType);
    }

    private void initJsonMapper() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }
}
