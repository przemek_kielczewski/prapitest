package com.example.mapper;

import com.example.JsonObjects.ApiResponse;

import java.io.IOException;

/**
 * Created by przemek on 2/4/15.
 */
public class ApiResponseMapper extends ResponseMapper {

    public ApiResponse map(String content) {
        try {
            return map(content, ApiResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            return new ApiResponse();
            //fail jak nie odczyta?
        }
    }
}
