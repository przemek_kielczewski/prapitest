package com.example.JsonObjects.ProductTypesTest;

import com.example.Done.BoolConverter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by przemek on 12/31/14.
 */
public class TypeItem {

    private Integer id;
    @JsonDeserialize(converter=BoolConverter.class)
    @JsonProperty("default")
    private Boolean defaultType;
    @JsonDeserialize(converter=BoolConverter.class)
    @JsonProperty("is_default")
    private Boolean defaultValue;
    private String name;
    @JsonProperty("attributes")
    private List<attributes> attributesList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean isDefaultType() {
        return defaultType;
    }

    public void setDefaultType(Boolean defaultType) {
        this.defaultType = defaultType;
    }

    public Boolean isDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Boolean isDefault) {
        this.defaultValue = isDefault;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<attributes> getAttributesList() {
        return attributesList;
    }

    public void setAttributesList(List<attributes> attributesList) {
        this.attributesList = attributesList;
    }





}
