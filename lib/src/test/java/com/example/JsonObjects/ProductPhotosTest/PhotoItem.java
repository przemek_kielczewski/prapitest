package com.example.JsonObjects.ProductPhotosTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/12/15.
 */
public class PhotoItem {

    Integer id;
    String image;
    @JsonProperty("product_id")
    Integer productId;
    @JsonProperty("real_product_id")
    Integer realProductId;
    @JsonProperty("is_main")
    Boolean isMain;
    @JsonProperty("retailer_product_id")
    Integer retailerProductId;
    String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getRealProductId() {
        return realProductId;
    }

    public void setRealProductId(Integer realProductId) {
        this.realProductId = realProductId;
    }

    public Boolean getIsMain() {
        return isMain;
    }

    public void setIsMain(Boolean isMain) {
        this.isMain = isMain;
    }

    public Integer getRetailerProductId() {
        return retailerProductId;
    }

    public void setRetailerProductId(Integer retailerProductId) {
        this.retailerProductId = retailerProductId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
