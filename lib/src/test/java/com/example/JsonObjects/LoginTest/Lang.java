package com.example.JsonObjects.LoginTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 12/24/14.
 */
public class Lang {

    private String lang;
    @JsonProperty("api_key")
    private String apiKey;

    public String getLang() {
        return lang;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String toString() {
        return "lang: " + lang + ", apiKey: " + apiKey;
    }

}
