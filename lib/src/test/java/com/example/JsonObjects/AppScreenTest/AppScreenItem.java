package com.example.JsonObjects.AppScreenTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/27/15.
 */
public class AppScreenItem {
    Integer id;
    @JsonProperty("autoloop")
    Integer autoLoop;
    String title;
    String subtitle;
    String description;
    Boolean home;
    String searchCriteria;
    @JsonProperty("media")
    ArrayList<ScreenMedia> screenMedia;
    Integer mc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAutoLoop() {
        return autoLoop;
    }

    public void setAutoLoop(Integer autoLoop) {
        this.autoLoop = autoLoop;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getHome() {
        return home;
    }

    public void setHome(Boolean home) {
        this.home = home;
    }

    public String getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(String searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public ArrayList<ScreenMedia> getScreenMedia() {
        return screenMedia;
    }

    public void setScreenMedia(ArrayList<ScreenMedia> screenMedia) {
        this.screenMedia = screenMedia;
    }

    public Integer getMc() {
        return mc;
    }

    public void setMc(Integer mc) {
        this.mc = mc;
    }
}
