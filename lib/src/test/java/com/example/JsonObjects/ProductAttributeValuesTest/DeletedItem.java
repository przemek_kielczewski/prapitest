package com.example.JsonObjects.ProductAttributeValuesTest;

/**
 * Created by przemek on 1/16/15.
 */
public class DeletedItem {

    private Integer id;


    public Integer getId() {
        return id;
    }

    public String toString() {
        return "id: " + id;
    }
}
