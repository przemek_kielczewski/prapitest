package com.example.JsonObjects.ProductAttributeValuesTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/14/15.
 */
public class AttributeValuesItem {

    Integer id;
    @JsonProperty("attribute_id")
    Integer attributeId;
    String image;
    String additional;
    String position;
    String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
