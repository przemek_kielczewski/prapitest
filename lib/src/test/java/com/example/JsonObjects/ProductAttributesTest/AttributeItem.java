package com.example.JsonObjects.ProductAttributesTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 12/31/14.
 */
public class AttributeItem {

    private Integer id;
    private String symbol;
    private Integer position;
    @JsonProperty("value_prefix")
    private String valuePrefix;
    @JsonProperty("value_sufix")
    private String valueSufix;
    private String name;
    @JsonProperty("type")
    private String attributeType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getValuePrefix() {
        return valuePrefix;
    }

    public void setValuePrefix(String valuePrefix) {
        this.valuePrefix = valuePrefix;
    }

    public String getValueSufix() {
        return valueSufix;
    }

    public void setValueSufix(String valueSufix) {
        this.valueSufix = valueSufix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }
}
