package com.example.JsonObjects;


/**
 * Created by przemek on 2/2/15.
 */
public class ApiResponse {
    String status;
    String message;
    String code;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isErrorWithCode(String code) {
        return ("ERROR").equals(getStatus()) && code.equals(getCode());
    }

    public boolean isOk() {
        return "OK".equals(getStatus());
    }
}
