package com.example.JsonObjects.CheckLicenseTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/22/15.
 */
public class CheckLicense {
    @JsonProperty("expiration_ts")
    Integer expiriationTs;
    @JsonProperty("expiration_date")
    String expiriationDate;
    @JsonProperty("still_valid")
    Boolean stillValid;

    public Integer getExpiriationTs() {
        return expiriationTs;
    }

    public void setExpiriationTs(Integer expiriationTs) {
        this.expiriationTs = expiriationTs;
    }

    public String getExpiriationDate() {
        return expiriationDate;
    }

    public void setExpiriationDate(String expiriationDate) {
        this.expiriationDate = expiriationDate;
    }

    public Boolean getStillValid() {
        return stillValid;
    }

    public void setStillValid(Boolean stillValid) {
        this.stillValid = stillValid;
    }
}
