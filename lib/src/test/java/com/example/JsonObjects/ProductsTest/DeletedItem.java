package com.example.JsonObjects.ProductsTest;

/**
 * Created by przemek on 12/31/14.
 */
public class DeletedItem {

    private Integer id;


    public Integer getId() {
        return id;
    }

    public String toString() {
        return "id: " + id;
    }
}
