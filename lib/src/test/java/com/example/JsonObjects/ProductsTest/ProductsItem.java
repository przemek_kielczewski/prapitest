package com.example.JsonObjects.ProductsTest;

import com.example.Done.BoolConverter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by przemek on 12/31/14.
 */
public class ProductsItem {
    private Integer id;
    @JsonProperty("product_id")
    private Integer productId;
    @JsonProperty("retailer_id")
    private Integer retailerId;
    @JsonProperty("external_id")
    private String externalId;
    @JsonDeserialize(converter=BoolConverter.class)
    @JsonProperty("hide_inherited_images")
    private Boolean hideInheritedImages;
    private String active;
    @JsonProperty("only_for_related")
    private boolean onlyForRelated;
    private boolean complete;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId) {
        this.retailerId = retailerId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Boolean isHideInheritedImages() {
        return hideInheritedImages;
    }

    public void setHideInheritedImages(Boolean hideInheritedImages) {
        this.hideInheritedImages = hideInheritedImages;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public boolean isOnlyForRelated() {
        return onlyForRelated;
    }

    public void setOnlyForRelated(boolean onlyForRelated) {
        this.onlyForRelated = onlyForRelated;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
