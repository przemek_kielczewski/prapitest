package com.example.JsonObjects.AppScreenListTest;

import com.example.JsonObjects.AppScreenTest.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/27/15.
 */
public class AppScreenMain {

    @JsonProperty("retailer_id")
    Integer retailerId;
    @JsonProperty("items")
    ArrayList<AppScreenItem> ScreenItem;
    Integer mc;
    Integer generation_ts;
    String lang;

    public Integer getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId) {
        this.retailerId = retailerId;
    }

    public ArrayList<AppScreenItem> getScreenItem() {
        return ScreenItem;
    }

    public void setScreenItem(ArrayList <AppScreenItem> screenItem) {
        ScreenItem = screenItem;
    }

    public Integer getMc() {
        return mc;
    }

    public void setMc(Integer mc) {
        this.mc = mc;
    }

    public Integer getGeneration_ts() {
        return generation_ts;
    }

    public void setGeneration_ts(Integer generation_ts) {
        this.generation_ts = generation_ts;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
