package com.example.JsonObjects.ProductVariationsTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/26/15.
 */
public class Attributes {

    @JsonProperty("attribute_id")
    Integer attributeId;
    @JsonProperty("textvalue")
    String textValue;
    private ArrayList<Integer> values;

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public ArrayList<Integer> getValues() {
        return values;
    }

    public void setValues(ArrayList<Integer> values) {
        this.values = values;
    }
}
