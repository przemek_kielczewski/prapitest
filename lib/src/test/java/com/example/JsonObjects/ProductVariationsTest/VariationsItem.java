package com.example.JsonObjects.ProductVariationsTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/23/15.
 */
public class VariationsItem {

    Integer id;
    @JsonProperty("retailer_product_id")
    Integer retailerProductId;
    @JsonProperty("attributes")
    ArrayList<Attributes> attributeItems;
    String type;

    public Integer getId() { return id; }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRetailerProductId() {
        return retailerProductId;
    }

    public void setRetailerProductId(Integer retailerProductId) {
        this.retailerProductId = retailerProductId;
    }

    public ArrayList<Attributes> getAttributeItems() {
        return attributeItems;
    }

    public void setAttributeItems(ArrayList<Attributes> attributeItems) {
        this.attributeItems = attributeItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
