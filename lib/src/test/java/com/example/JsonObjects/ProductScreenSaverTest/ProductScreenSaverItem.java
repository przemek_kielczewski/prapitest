package com.example.JsonObjects.ProductScreenSaverTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/20/15.
 */
public class ProductScreenSaverItem {


    Integer id;
    @JsonProperty("retailer_id")
    Integer retailerId;
    Integer position;
    String image;
    Integer delay;
    @JsonProperty("animation_in")
    String animationIn;
    @JsonProperty("animation_out")
    String animationOut;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId) {
        this.retailerId = retailerId;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    public String getAnimationIn() {
        return animationIn;
    }

    public void setAnimationIn(String animationIn) {
        this.animationIn = animationIn;
    }

    public String getAnimationOut() {
        return animationOut;
    }

    public void setAnimationOut(String animationOut) {
        this.animationOut = animationOut;
    }




}
