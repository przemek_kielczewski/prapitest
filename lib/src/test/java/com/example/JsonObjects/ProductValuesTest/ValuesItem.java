package com.example.JsonObjects.ProductValuesTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by przemek on 1/14/15.
 */
public class ValuesItem {

    Integer id;
    @JsonProperty("retailer_product_id")
    Integer retailerProductId;
    @JsonProperty("attribute_id")
    Integer attributeId;
    @JsonProperty ("textvalue")
    String textValue;
    private ArrayList <Integer> values;
    String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRetailerProductId() {
        return retailerProductId;
    }

    public void setRetailerProductId(Integer retailerProductId) {
        this.retailerProductId = retailerProductId;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public ArrayList<Integer> getValues() {
        return values;
    }

    public void setValues(ArrayList<Integer> values) {
        this.values = values;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
