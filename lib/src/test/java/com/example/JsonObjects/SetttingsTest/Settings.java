package com.example.JsonObjects.SetttingsTest;

import com.example.JsonObjects.LoginTest.*;
import com.example.JsonObjects.ProductInspirationTest.DeletedItem;
import com.example.JsonObjects.ProductInspirationTest.ProductInspirationItem;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by przemek on 1/21/15.
 */
public class Settings {
    private String status;
    private Integer code;
    @JsonProperty("api_key")
    private String apiKey;
    private String lang;
    private List<Lang> langs;
    private String Username;
    @JsonProperty("retailer_id")
    private Integer retailerId;
    @JsonProperty("main_bg_color")
    private String mainBgColor;
    @JsonProperty("button_color")
    private String buttonColor;
    @JsonProperty("big_logo")
    private String bigLogo;
    @JsonProperty("small_logo")
    private String smallLogo;
    private String retailer;
    @JsonProperty("show_price")
    private String showPrice;
    private String currency;
    @JsonProperty("pin_code")
    private String pinCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<Lang> getLangs() {
        return langs;
    }

    public void setLangs(List<Lang> langs) {
        this.langs = langs;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public Integer getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId) {
        this.retailerId = retailerId;
    }

    public String getMainBgColor() {
        return mainBgColor;
    }

    public void setMainBgColor(String mainBgColor) {
        this.mainBgColor = mainBgColor;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(String buttonColor) {
        this.buttonColor = buttonColor;
    }

    public String getBigLogo() {
        return bigLogo;
    }

    public void setBigLogo(String bigLogo) {
        this.bigLogo = bigLogo;
    }

    public String getSmallLogo() {
        return smallLogo;
    }

    public void setSmallLogo(String smallLogo) {
        this.smallLogo = smallLogo;
    }

    public String getRetailer() {
        return retailer;
    }

    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }

    public String getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(String showPrice) {
        this.showPrice = showPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}
