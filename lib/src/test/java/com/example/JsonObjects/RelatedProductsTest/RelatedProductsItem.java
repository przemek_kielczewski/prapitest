package com.example.JsonObjects.RelatedProductsTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/22/15.
 */
public class RelatedProductsItem {

    Integer id;
    @JsonProperty("relation_id")
    Integer relationId;
    @JsonProperty("db_parent_id")
    Integer dbParentId;
    @JsonProperty("dp_parent_product_id")
    Integer dpParentProductId;
    @JsonProperty("parent_id")
    Integer parentId;
    @JsonProperty("db_child_id")
    Integer dbChildId;
    @JsonProperty("dp_child_product_id")
    Integer dpChildProductId;
    @JsonProperty("child_id")
    Integer childId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRelationId() {
        return relationId;
    }

    public void setRelationId(Integer relationId) {
        this.relationId = relationId;
    }

    public Integer getDbParentId() {
        return dbParentId;
    }

    public void setDbParentId(Integer dbParentId) {
        this.dbParentId = dbParentId;
    }

    public Integer getDpParentProductId() {
        return dpParentProductId;
    }

    public void setDpParentProductId(Integer dpParentProductId) {
        this.dpParentProductId = dpParentProductId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getDbChildId() {
        return dbChildId;
    }

    public void setDbChildId(Integer dbChildId) {
        this.dbChildId = dbChildId;
    }

    public Integer getDpChildProductId() {
        return dpChildProductId;
    }

    public void setDpChildProductId(Integer dpChildProductId) {
        this.dpChildProductId = dpChildProductId;
    }

    public Integer getChildId() {
        return childId;
    }

    public void setChildId(Integer childId) {
        this.childId = childId;
    }
}
