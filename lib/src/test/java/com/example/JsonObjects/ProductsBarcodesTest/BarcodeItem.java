package com.example.JsonObjects.ProductsBarcodesTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/7/15.
 */
public class BarcodeItem {

    Integer id;
    String barcode;
    Integer pid;
    @JsonProperty("real_product_id")
    Integer realProductId;
    @JsonProperty("retailer_product_id")
    Integer retailerProductId;
    String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getRealProductId() {
        return realProductId;
    }

    public void setRealProductId(Integer realProductId) {
        this.realProductId = realProductId;
    }

    public Integer getRetailerProductId() {
        return retailerProductId;
    }

    public void setRetailerProductId(Integer retailerProductId) {
        this.retailerProductId = retailerProductId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
