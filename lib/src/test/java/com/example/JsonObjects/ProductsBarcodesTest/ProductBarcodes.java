package com.example.JsonObjects.ProductsBarcodesTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by przemek on 1/7/15.
 */
public class ProductBarcodes {

    private String status;
    private List<BarcodeItem> items;
    private List<DeletedItem> deleted;
    private Integer total;
    @JsonProperty("z_duration")
    private Float zDuration;
    private Integer time;
    private Integer records;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BarcodeItem> getItems() {
        return items;
    }

    public void setItems(List<BarcodeItem> items) {
        this.items = items;
    }

    public List<DeletedItem> getDeletedItems() {
        return deleted;
    }

    public void setDeletedItems(List<DeletedItem> deleted) {
        this.deleted = deleted;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Float getzDuration() {
        return zDuration;
    }

    public void setzDuration(Float zDuration) {
        this.zDuration = zDuration;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }
}
