package com.example.JsonObjects.ProductInspirationTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/21/15.
 */
public class ProductInspirationElement {

    int id;
    String image;
    int position;
    @JsonProperty("tab_id")
    int tabId;
    @JsonProperty("value_id")
    int valueId;
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTabId() {
        return tabId;
    }

    public void setTabId(int tabId) {
        this.tabId = tabId;
    }

    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
