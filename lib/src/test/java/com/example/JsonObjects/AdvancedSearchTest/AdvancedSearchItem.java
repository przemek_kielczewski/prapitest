package com.example.JsonObjects.AdvancedSearchTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/21/15.
 */
public class AdvancedSearchItem {
    Integer id;
    @JsonProperty("retailer_id")
    Integer retailerId;
    String name;
    @JsonProperty("elements")
    ArrayList<AdvancedSearchElement> List;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId) {
        this.retailerId = retailerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<AdvancedSearchElement> getList() {
        return List;
    }

    public void setList(ArrayList<AdvancedSearchElement> list) {
        List = list;
    }
}
