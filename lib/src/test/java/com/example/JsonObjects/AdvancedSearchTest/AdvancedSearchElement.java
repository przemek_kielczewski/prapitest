package com.example.JsonObjects.AdvancedSearchTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/22/15.
 */
public class AdvancedSearchElement {

    Integer id;
    @JsonProperty("section_id")
    Integer sectionId;
    @JsonProperty("attribute_id")
    Integer attributeId;
    Integer position;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
