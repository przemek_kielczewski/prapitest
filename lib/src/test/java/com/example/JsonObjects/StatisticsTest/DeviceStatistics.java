package com.example.JsonObjects.StatisticsTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/23/15.
 */
public class DeviceStatistics {

    @JsonProperty("searches_by_phrase")
    private ArrayList<RetailerPhrase> retailerPhrase;
    @JsonProperty("searches_by_attribute")
    private ArrayList<RetailerAttribute> retailerAttribute;
    @JsonProperty("product_views")
    private ArrayList<RetailerView> retailerView;
    @JsonProperty("product_scans")
    private ArrayList<RetailerScan> retailerScan;
    @JsonProperty("active_hours")
    private ArrayList<RetailerHours> retailerHours;

    public ArrayList<RetailerPhrase> getRetailerPhrase() {
        return retailerPhrase;
    }

    public void setRetailerPhrase(ArrayList<RetailerPhrase> retailerPhrase) {
        this.retailerPhrase = retailerPhrase;
    }

    public ArrayList<RetailerAttribute> getRetailerAttribute() {
        return retailerAttribute;
    }

    public void setRetailerAttribute(ArrayList<RetailerAttribute> retailerAttribute) {
        this.retailerAttribute = retailerAttribute;
    }

    public ArrayList<RetailerView> getRetailerView() {
        return retailerView;
    }

    public void setRetailerView(ArrayList<RetailerView> retailerView) {
        this.retailerView = retailerView;
    }

    public ArrayList<RetailerScan> getRetailerScan() {
        return retailerScan;
    }

    public void setRetailerScan(ArrayList<RetailerScan> retailerScan) {
        this.retailerScan = retailerScan;
    }

    public ArrayList<RetailerHours> getRetailerHours() {
        return retailerHours;
    }

    public void setRetailerHours(ArrayList<RetailerHours> retailerHours) {
        this.retailerHours = retailerHours;
    }
}
