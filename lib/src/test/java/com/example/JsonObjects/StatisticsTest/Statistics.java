package com.example.JsonObjects.StatisticsTest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by przemek on 1/23/15.
 */
public class Statistics {

    private String status;
    private Totals totals;
    @JsonProperty("retailer_statistics")
    private RetailerStatistics retailerStatistics;
    @JsonProperty("device_statistics")
    private DeviceStatistics deviceStatistics;
    @JsonProperty("z_duration")
    private Float zDuration;
    private Integer time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Totals getTotals() {
        return totals;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }

    public RetailerStatistics getRetailerStatistics() {
        return retailerStatistics;
    }

    public void setRetailerStatistics(RetailerStatistics retailerStatistics) {
        this.retailerStatistics = retailerStatistics;
    }

    public DeviceStatistics getDeviceStatistics() {
        return deviceStatistics;
    }

    public void setDeviceStatistics(DeviceStatistics deviceStatistics) {
        this.deviceStatistics = deviceStatistics;
    }

    public Float getzDuration() {
        return zDuration;
    }

    public void setzDuration(Float zDuration) {
        this.zDuration = zDuration;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
