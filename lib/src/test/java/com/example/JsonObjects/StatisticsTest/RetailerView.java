package com.example.JsonObjects.StatisticsTest;

/**
 * Created by przemek on 1/23/15.
 */
public class RetailerView {
    String param;
    int occurance;

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public int getOccurance() {
        return occurance;
    }

    public void setOccurance(int occurance) {
        this.occurance = occurance;
    }
}
