package com.example.JsonObjects.StatisticsTest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by przemek on 1/23/15.
 */
public class Totals {
    @JsonProperty("searches_by_phrase")
    private Integer searchesByPhrase;
    @JsonProperty("searches_by_attribute")
    private Integer searchesByAttribute;
    @JsonProperty("user_sessions")
    private Integer userSessions;
    @JsonProperty("product_views")
    private Integer productViews;

    public Integer getSearchesByPhrase() {
        return searchesByPhrase;
    }

    public void setSearchesByPhrase(Integer searchesByPhrase) {
        this.searchesByPhrase = searchesByPhrase;
    }

    public Integer getSearchesByAttribute() {
        return searchesByAttribute;
    }

    public void setSearchesByAttribute(Integer searchesByAttribute) {
        this.searchesByAttribute = searchesByAttribute;
    }

    public Integer getUserSessions() {
        return userSessions;
    }

    public void setUserSessions(Integer userSessions) {
        this.userSessions = userSessions;
    }

    public Integer getProductViews() {
        return productViews;
    }

    public void setProductViews(Integer productViews) {
        this.productViews = productViews;
    }
}
