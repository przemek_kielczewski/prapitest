package com.example.JsonObjects.StatisticsTest;

/**
 * Created by przemek on 1/23/15.
 */
public class RetailerScan {

    Integer occurance;
    Integer param;

    public Integer getOccurance() {
        return occurance;
    }

    public void setOccurance(Integer occurance) {
        this.occurance = occurance;
    }

    public Integer getParam() {
        return param;
    }

    public void setParam(Integer param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return "RetailerScan{" +
                "occurance=" + occurance +
                ", param=" + param +
                '}';
    }
}
