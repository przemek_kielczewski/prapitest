package com.example;

import java.util.Random;

/**
 * Created by przemek on 2/11/15.
 */
public class Randomizer {

    Random rand = new Random();

    //to można zrandomować:
    //incorrectapikey tak
    //screen id tak
    //email ?
    //agremment tak
    //mid ?
    //productID tak
    //username tak
    //password tak
    //registrationid tak
    //datefrom ?
    //dateto ?
    //timestamp losowy teraz, losowy kiedyś, losowy przyszłość tak
    //data


    public String makeRandomAgreement() {
        String numbers = "023456789";
        char[] randomAgreement = new char[3];
        for (int i = 0; i < 3; i++) {
            randomAgreement[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(randomAgreement);
    }

    public String makeRandomRegistrationid() {
        String numbers = "023456789";
        char[] randomRegistrationid = new char[5];
        for (int i = 0; i < 5; i++) {
            randomRegistrationid[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(randomRegistrationid);
    }

    public String makeRandomScreenId() {
        String numbers = "0123456789";
        char[] randomScreenId = new char[3];
        for (int i = 0; i < 3; i++) {
            randomScreenId[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(randomScreenId);
    }

    public String makeRandomProductId() {
        String numbers = "0123456789";
        char[] randomScreenId = new char[2];
        for (int i = 0; i < 2; i++) {
            randomScreenId[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(randomScreenId);
    }


    public static String makeRandomApiKey() {
        Random rand = new Random();
        String numbers = "a0b1c2d3e4f56789";
        char[] randomApiKey = new char[20];
        for (int i = 0; i < 20; i++) {
            randomApiKey[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(randomApiKey);
    }

    public String makeRandomUser() {
        String numbers = "abcdefghijklmnoprstuwz";
        char[] randomUser = new char[9];
        for (int i = 0; i < 9; i++) {
            randomUser[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(randomUser);
    }


    public String makeRandomPastTimeStamp() {

        String numbers = "0123456789";
        char[] PastTimeStamp = new char[10];
        PastTimeStamp [0] = numbers.charAt(9);
        PastTimeStamp [1] = numbers.charAt(5);
        for (int i = 2; i < 10; i++) {
            PastTimeStamp[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(PastTimeStamp);
    }

    public String makeRandomRecentTimeStamp() {

        String numbers = "0123456789";
        char[] recentTimeStamp = new char[10];
        recentTimeStamp [0] = numbers.charAt(1);
        recentTimeStamp [1] = numbers.charAt(2);
        recentTimeStamp [2] = numbers.charAt(9);
        recentTimeStamp [3] = numbers.charAt(5);
        for (int i = 4; i < 10; i++) {
            recentTimeStamp[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(recentTimeStamp);
    }

    public String makeRandomFutureTimeStamp() {

        String numbers = "0123456789";
        char[] FutureTimeStamp = new char[10];
        FutureTimeStamp [0] = numbers.charAt(2);
        FutureTimeStamp [1] = numbers.charAt(8);
        for (int i = 2; i < 10; i++) {
            FutureTimeStamp[i] = numbers.charAt(rand.nextInt(numbers.length()));
        }
        return new String(FutureTimeStamp);
    }


}
