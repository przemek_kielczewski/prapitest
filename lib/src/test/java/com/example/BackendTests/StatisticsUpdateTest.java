package com.example.BackendTests;

import com.example.ConnectionMaker;
import com.example.JsonObjects.StatisticsTest.DeviceStatistics;
import com.example.JsonObjects.StatisticsTest.RetailerStatistics;
import com.example.JsonObjects.StatisticsTest.Statistics;
import com.example.JsonObjects.StatisticsTest.Totals;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by przemek on 1/30/15.
 */
public class StatisticsUpdateTest {

    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";
    public static final String REGISTRATION_ID =
            "pQpIoVjVXvybpKuZ3PZxGko4PfCB2QEuM3Garg7ea8XUIXA8hHyGoEBRvB7CrxqIAI6HETx_00EOUxxsT9IX65bshP2CqHaVckL-8iTpOi4gz6naFQiEfl83TwzZ6g3ztM6O_31l33bpcqJyTcn2gCQ";
    public static final String DATE_FROM = "1293793200";
    public static final String DATE_TO = "1422609692";
    public static final String DATE_TO2 = "1422873009";
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();


    @Test
    public void StatisticsUpdateTest1() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("registration_id", REGISTRATION_ID));
        keyValue.add(new BasicNameValuePair("date_from", DATE_FROM));
        keyValue.add(new BasicNameValuePair("date_to", DATE_TO));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/stat/index", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Statistics statistics = new ResponseMapper().map(content, Statistics.class);

            assertTrue("Status field incorrect", statistics.getStatus().equals("OK")
                    || statistics.getStatus().equals("ERROR"));
            assertNotNull("Totals Collection is null", statistics.getTotals());
            assertNotNull("Retailer Statistics Collection is null", statistics.getRetailerStatistics());
            assertNotNull("Device Statistics is null", statistics.getDeviceStatistics());

            if (statistics != null) {
                Totals totals = statistics.getTotals();

                assertNotNull("search By Phrase field is null", totals.getSearchesByPhrase());
                assertNotNull("search By Attribute field is null", totals.getSearchesByAttribute());
                assertNotNull("search By usersessions field is null", totals.getUserSessions());
                assertNotNull("search by productviews", totals.getProductViews());
                //totals

                RetailerStatistics retailerStatistics = statistics.getRetailerStatistics();
                assertNotNull("search By Phrase field is null", retailerStatistics.getRetailerPhrase());
                assertNotNull("search By Attribute field is null", retailerStatistics.getRetailerAttribute());
                assertNotNull("product Views field is null", retailerStatistics.getRetailerView());
                assertNotNull("product Scans is null", retailerStatistics.getRetailerScan());
                assertNotNull("Active hours is null", retailerStatistics.getRetailerHours());

                DeviceStatistics deviceStatistics = statistics.getDeviceStatistics();
                assertNotNull("search By Phrase field is null", deviceStatistics.getRetailerPhrase());
                assertNotNull("search By Attribute field is null", deviceStatistics.getRetailerAttribute());
                assertNotNull("product Views field is null", deviceStatistics.getRetailerView());
                assertNotNull("product Scans is null", deviceStatistics.getRetailerScan());
                assertNotNull("Active hours is null", deviceStatistics.getRetailerHours());

                System.out.println(deviceStatistics.getRetailerScan());

            } else {
                fail("Products is null");
            }

            assertNotNull("zDuration field is null", statistics.getzDuration());
            //czy nie jest strasznie duże to duration ale to potem
            assertNotNull("TimeStamp field is null", statistics.getTime());

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }


    }

    @Test
    public void StatisticsUpdateTest2() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("registration_id", REGISTRATION_ID));
        keyValue.add(new BasicNameValuePair("date_from", DATE_FROM));
        keyValue.add(new BasicNameValuePair("date_to", DATE_TO2));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/stat/index", keyValue);

        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Statistics statistics = new ResponseMapper().map(content, Statistics.class);

            assertTrue("Status field incorrect", statistics.getStatus().equals("OK")
                    || statistics.getStatus().equals("ERROR"));
            assertNotNull("Totals Collection is null", statistics.getTotals());
            assertNotNull("Retailer Statistics Collection is null", statistics.getRetailerStatistics());
            assertNotNull("Device Statistics is null", statistics.getDeviceStatistics());

            if (statistics != null) {
                Totals totals = statistics.getTotals();

                assertNotNull("search By Phrase field is null", totals.getSearchesByPhrase());
                assertNotNull("search By Attribute field is null", totals.getSearchesByAttribute());
                assertNotNull("search By usersessions field is null", totals.getUserSessions());
                assertNotNull("search by productviews", totals.getProductViews());
                //totals

                RetailerStatistics retailerStatistics = statistics.getRetailerStatistics();
                assertNotNull("search By Phrase field is null", retailerStatistics.getRetailerPhrase());
                assertNotNull("search By Attribute field is null", retailerStatistics.getRetailerAttribute());
                assertNotNull("product Views field is null", retailerStatistics.getRetailerView());
                assertNotNull("product Scans is null", retailerStatistics.getRetailerScan());
                assertNotNull("Active hours is null", retailerStatistics.getRetailerHours());

                DeviceStatistics deviceStatistics = statistics.getDeviceStatistics();
                assertNotNull("search By Phrase field is null", deviceStatistics.getRetailerPhrase());
                assertNotNull("search By Attribute field is null", deviceStatistics.getRetailerAttribute());
                assertNotNull("product Views field is null", deviceStatistics.getRetailerView());
                assertNotNull("product Scans is null", deviceStatistics.getRetailerScan());
                assertNotNull("Active hours is null", deviceStatistics.getRetailerHours());

                System.out.println(deviceStatistics.getRetailerScan());
            } else {
                fail("Products is null");
            }

            assertNotNull("zDuration field is null", statistics.getzDuration());
            //czy nie jest strasznie duże to duration ale to potem
            assertNotNull("TimeStamp field is null", statistics.getTime());

        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");
        }


    }
}