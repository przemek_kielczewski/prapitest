package com.example.BackendTests;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ProductValuesTest.DeletedItem;
import com.example.JsonObjects.ProductValuesTest.ProductValues;
import com.example.JsonObjects.ProductValuesTest.ValuesItem;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by przemek on 1/29/15.
 */
public class BackendValUpdateTest {

    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";
    public static final String TIMESTAMP = "1422527000";
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();


        @Test
        public void BackendValUpdateTest1() throws IOException {
            keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
            keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
            String content = connectionMaker.Connect
                    ("http://app.peak-retail.com/web/api.php/product/productValues", keyValue);
            try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
                ProductValues productValues =
                        new ResponseMapper().map(content, ProductValues.class);
                assertTrue("Status field incorrect", productValues.getStatus().equals("OK")
                        || productValues.getStatus().equals("ERROR"));
                assertNotNull("Products Collection is null", productValues.getItems());

                boolean updated = false;

                if (productValues != null) {

                    for (ValuesItem valuesItem : productValues.getItems()) {

                        if (valuesItem.getAttributeId() == 19 && valuesItem.getTextValue().equals("Wielki Dagon")) {
                            updated = true;
                        }
                    }
                    assertTrue("value did not update", updated);

                    for (DeletedItem deletedItem : productValues.getDeletedItems()) {
                        assertNotNull("DeletedItem id is null", deletedItem.getId());
                    }

                    assertNotNull("zDuration field is null", productValues.getzDuration());
                    //czy nie jest strasznie duże to duration, ale to potem
                    assertNotNull("TimeStamp field is null", productValues.getTime());
                    assertTrue("Records field incorrect", productValues.getItems().size() == productValues.getRecords());
                } else {
                    fail("Products is null");
                    fail("parsing error");

                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }
