package com.example.BackendTests;

import com.example.ConnectionMaker;
import com.example.JsonObjects.ProductsTest.DeletedItem;
import com.example.JsonObjects.ProductsTest.Products;
import com.example.JsonObjects.ProductsTest.ProductsItem;
import com.example.mapper.ResponseMapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by przemek on 1/29/15.
 */
public class BackendInheritedDeletedTest {

    public static final String CORRECT_API_KEY = "8b8569745eb411e49929";
    public static final String TIMESTAMP = "1422527000";
    //preparations
    ArrayList<NameValuePair> keyValue = new ArrayList<NameValuePair>();
    ConnectionMaker connectionMaker = new ConnectionMaker();

    //preparations
    int deleted = 23496;

    //tests
    @Test
    public void BackendDeleteInheritedTest() throws IOException {
        keyValue.add(new BasicNameValuePair("api_key", CORRECT_API_KEY));
        keyValue.add(new BasicNameValuePair("timestamp", TIMESTAMP));
        String content = connectionMaker.Connect
                ("http://app.peak-retail.com/web/api.php/product/products", keyValue);
        try {//failed on unknown properties - jeśli coś dojdzie w api będziemy wiedzieć.
            Products products = new ResponseMapper().map(content, Products.class);

            assertTrue("Status field incorrect", products.getStatus().equals("OK")
                    || products.getStatus().equals("ERROR"));
            assertNotNull("Products Collection is null", products.getItems());

            if (products != null) {

                for ( ProductsItem item: products.getItems()) {
                    assertNotNull("id field is null", item.getId()); //obowiązkowe
                    assertNotNull("product id field is null", item.getProductId()); //obowiązkowe
                    assertNotNull("retailer id is null", item.getRetailerId()); //obowiązkowe
                    assertNotNull("external id is null", item.getExternalId()); //external_id może być pusty
                    assertNotNull("HideInheritedImages is null", item.isHideInheritedImages());
                    //obowiązkowe true lub false
                    assertNotNull("active field is null", item.getActive());
                    assertTrue("active field error", item.getActive().equals("true") || item.getActive().equals("false"));
                    assertNotNull("OnlyForRelated is null", item.isOnlyForRelated()); //obowiązkowe true lub false
                    assertNotNull("complete field is null", item.isComplete()); //obowiązkowe true lub false
                    assertNotNull("Type field is empty", item.getType()); //obowiązkowe String

                }
                boolean deletedFound = false;

                for (DeletedItem deletedItem: products.getDeletedItems()) {
                    assertNotNull("DeletedItem id is null", deletedItem.getId());

                    if (deletedItem.getId() == deleted) {
                        deletedFound=true;
                    }
                }
                assertTrue("Product is not in deleted", deletedFound);

                assertNotNull("zDuration field is null", products.getzDuration());
                //czy nie jest strasznie duże to duration ale to potem
                assertNotNull("TimeStamp field is null", products.getTime());
                assertTrue("Records field incorrect", products.getItems().size() == products.getRecords());

            } else {
                fail("Products is null");
            }


        } catch (IOException e1) {
            e1.printStackTrace();
            fail("parsing error");

        }

    }
}
