package com.example;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by przemek on 2/11/15.
 */
public class ConnectionMaker {

    public String Connect(String wwwSite, ArrayList arrayList) throws IOException {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(wwwSite);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        CloseableHttpResponse response = httpclient.execute(httpPost);
        String responseString = EntityUtils.toString(response.getEntity());
        return responseString;
    }
}
