package com.example;

import com.example.Done.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by przemek on 1/30/15.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        AdvancedSearchTest.class,
        AppScreenListTest.class,
        AppScreenTest.class,
        CheckLicenseTest.class,
        EmailAttachmentTest.class,
        EmailPDFTest.class,
        EventStoreTest.class,
        HeartBeatTest.class,
        LoginTest.class,
        LogoutTest.class,
        OnlineStatusTest.class,
        ProductAttributesTest.class,
        ProductAttributeValuesTest.class,
        ProductBarcodesTest.class,
        ProductInspirationTest.class,
        ProductPhotosTest.class,
        ProductsTest.class,
        ProductTypesTest.class,
        ProductValuesTest.class,
        ProductVariationsTest.class,
        RelatedProductsTest.class,
        ScreenSaverTest.class,
        SettingsTest.class,
        StatisticsTest.class,
        SqLiteTest.class

})
public class TestSuite {

}
