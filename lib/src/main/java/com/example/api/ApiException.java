package com.example.api;

import android.content.Context;

import com.example.log.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ApiException extends Exception {

    private static final Logger LOG = new Logger(ApiException.class);
    private static final String TAG = ApiException.class.getSimpleName();

    private static final long serialVersionUID = 4429197136060598481L;

    public static final int ERROR_NO_INTERNET            = 1;
    public static final int ERROR_CONNECTION_RESET       = 2;
    public static final int ERROR_DATA_STRUCTURE_INVALID = 3;
    public static final int ERROR_ILLEGAL_ARGUMENT       = 4;
    public static final int ERROR_ILLEGAL_STATE          = 5;
    public static final int ERROR_IO_EXCEPTION           = 6;

    private int errorCode = -1;

    private String code;

    private List<Error> errors = new ArrayList<Error>();

    public ApiException() {
        super();
    }

    public ApiException(String err) {
        super(err);
    }

    public ApiException(JSONArray errors, String code) {
        super();
        this.code = code;
        this.errors = parseErrors(errors);
    }

    public void addError(String apiKey, String msg) {
        errors.add(new Error(apiKey, msg));
    }

    private List<Error> parseErrors(JSONArray array) {
        List<Error> errors = new ArrayList<Error>();
        if (array != null) {
            try {
                JSONObject object = array.getJSONObject(0);
                Iterator i = object.keys();
                while (i.hasNext()) {
                    String key = i.next().toString();
                    String message = object.get(key).toString();
                    errors.add(new Error(key, message));
                }

            } catch (JSONException e) {
                LOG.e(e);
            }
        }
        return errors;
    }

    public String getCode() {
        return code;
    }

    public ApiException(int code) {
        super();
    }

    public ApiException(String err, int code) {
        super(err);
        errorCode = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Returns message from resources.
     *
     * @param context
     * @return
     */
    public String getMessage(Context context) {
        if (errorCode < 0) {
            return getMessage();
        }
        int errorMessageId = context.getResources().getIdentifier("api_error_" + errorCode, "string",
                context.getPackageName());
        if (errorMessageId == 0) {
            return getMessage();
        }
        return context.getString(errorMessageId);
    }

    public List<Error> getErrors() {
        return errors;
    }

//    public boolean hasErrors() {
//        return CollectionUtils.isNotEmpty(errors);
//    }
}
