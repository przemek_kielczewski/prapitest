package com.example.api;

/**
 * @author Mateusz Strzelecki
 * @since 06.02.14
 */
public class Error {

    private String apiKey;

    private String title;

    private String message;

    public Error() {
    }

    public Error(String apiKey, String message) {
        this.apiKey = apiKey;
        this.message = message;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Error{" +
                "apiKey='" + apiKey + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
