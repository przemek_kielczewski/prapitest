package com.example.api.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.api.ApiException;
import com.example.log.Logger;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.StringEntity;
//import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;


/**
 * @author Krzysztof Wagner - k.wagner@foxcode.pl
 */
public class Http {

    private static final Logger LOG = new Logger(Http.class);

    public static final  String METHOD_POST  = "post";
    public static final  String METHOD_GET   = "get";
    private static final int    SERVER_DELAY = 10000;

    public static final String CONTENT_TYPE_XML         = "text/xml;charset=UTF-8";
    public static final String CONTENT_TYPE_HTML        = "text/html;charset=UTF-8";
    public static final String CONTENT_TYPE_JSON        = "application/json;charset=utf-8";
    public static final String CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded;charset=utf-8";

    private static final String USER_AGENT_CHROME         = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/30.0.1599.114 Chrome/30.0.1599.114 Safari/537.36";
    private static final String USER_AGENT_ANDROID_APACHE = "Apache-HttpClient/Android";

    private static final String TAG = Http.class.getSimpleName();

    private static Http instance;

    private Context mContext;
    private String  mCookies;

    public static Http getInstance() throws NotInitializedException {
        if (instance == null)
            throw new Http().new NotInitializedException();
        return instance;
    }

    private Http() {
    }

    private Http(Context context) {
        this.mContext = context;
    }

    public static Http initialize(Context context) {
        instance = new Http(context);
        return instance;
    }

    public void setCookiesHeader(String cookies) {
        this.mCookies = cookies;
    }

    public InputStream getIS(String url, String contentType) throws ApiException {
//		Log.d(TAG, url);
        log(url);
//        if (!isOnline())
//            throw new ApiException(null, ApiException.ERROR_NO_INTERNET);
        InputStream is = null;

        BasicCookieStore cookieStore = new BasicCookieStore();
        BasicHttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

        DefaultHttpClient httpClient = new DefaultHttpClient(prepareBasicHttpParams());
        httpClient.setCookieStore(cookieStore);
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);

        try {
            HttpGet httpget = new HttpGet(url);
            httpget.setHeader("Content-Type", contentType);
//            httpget.setHeader("token", ZabkaApp.getUserIdentifier());
            httpget.setHeader("Accept",
                    "text/html,text/xml,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            httpget.setHeader("Accept-Encoding", "gzip,deflate,sdch");
            httpget.setHeader("Accept-Language", "en-US,en;q=0.8,pl;q=0.6");
            if (mCookies != null && mCookies.length() > 0) {
                httpget.setHeader("Cookie", mCookies);
                mCookies = null;
            }

            HttpResponse response = httpClient.execute(httpget, httpContext);
            int statusCode = response.getStatusLine().getStatusCode();
            for (Header header : response.getAllHeaders()) {
                log(header.getName() + ": " + header.getValue());
            }
            log(response.getStatusLine().toString());
            if (statusCode != HttpStatus.SC_OK) {
                throw new ApiException(response.getStatusLine().getReasonPhrase(), statusCode);
            }
            is = response.getEntity().getContent();
            Header contentEncoding = response.getFirstHeader("Content-Encoding");
            if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                log("using gzip");
                is = new GZIPInputStream(is);
            }
        } catch (SocketException e) {
            throw new ApiException(e.getMessage(), ApiException.ERROR_CONNECTION_RESET);
        } catch (ClientProtocolException e) {
            throw new ApiException(e.getMessage());
        } catch (IOException e) {
            LOG.e(e);
            throw new ApiException(e.getMessage(), ApiException.ERROR_IO_EXCEPTION);
        } catch (IllegalArgumentException e) {
            throw new ApiException(e.getMessage(), ApiException.ERROR_ILLEGAL_ARGUMENT);
        } catch (IllegalStateException e) {
            throw new ApiException(e.getMessage(), ApiException.ERROR_ILLEGAL_STATE);
        }
        if (is == null) {
            return null;
        }
        return is;
    }

    public String get(String url, String contentType) throws ApiException {
        return convertStreamToString(getIS(url, contentType));
    }

    public String get(String url) throws ApiException {
        return get(url, "text/html;charset=UTF-8");
    }

    public InputStream getIS(String url) throws ApiException {
        return getIS(url, "text/html;charset=UTF-8");
    }

    public String post(String url, String post, String contentType) throws ApiException {
        return convertStreamToString(postIS(url, post, contentType));
    }

    public InputStream postIS(String url, String post, String contentType) throws ApiException {
//        if (!isOnline())
//            throw new ApiException(null, ApiException.ERROR_NO_INTERNET);
        InputStream is = null;
        try {
            HttpPost httppost = new HttpPost(url);
            StringEntity se = new StringEntity(post, HTTP.UTF_8);

            // se.setContentType("text/xml");
            httppost.setHeader("Content-Type", contentType);
//            httppost.setHeader("token", ZabkaApp.getUserIdentifier());
            httppost.setEntity(se);

            HttpResponse response = new DefaultHttpClient().execute(httppost);
            is = response.getEntity().getContent();
        } catch (Exception e) {
            LOG.e(e);
        }
        return is;
    }

    public InputStream post(String url, String post) throws ApiException {
        return postIS(url, post, "text/html;charset=UTF-8");
    }

    public String post(String url, Map<String, String> post, String contentType) throws ApiException {
//        if (!isOnline())
//            throw new ApiException(null, ApiException.ERROR_NO_INTERNET);

        String result = "";
        try {
            HttpPost httppost = new HttpPost(url);
            httppost.setHeader("Content-Type", contentType);
//            httppost.setHeader("token", ZabkaApp.getUserIdentifier());
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            for (String key : post.keySet())
                nameValuePairs.add(new BasicNameValuePair(key, post.get(key)));

            StringEntity se = new StringEntity(getQuery(nameValuePairs), HTTP.UTF_8);
            httppost.setEntity(se);

            HttpResponse response = new DefaultHttpClient().execute(httppost);

            InputStream in = response.getEntity().getContent();

            result = new Scanner(in).useDelimiter("\\A").next();

        } catch (UnsupportedEncodingException e) {
            throw new ApiException(e.getMessage());
        } catch (ClientProtocolException e) {
            throw new ApiException(e.getMessage());
        } catch (IOException e) {
            throw new ApiException(e.getMessage());
        }

        return result;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


//    public String multipartPost(String url, MultipartEntity reqEntity) throws ApiException {
//        if (!isOnline()) {
//            throw new ApiException(null, ApiException.ERROR_NO_INTERNET);
//        }
//        InputStream is = null;
//        try {
//            HttpPost postRequest = new HttpPost(url);
//            postRequest.setEntity(reqEntity);
//            HttpResponse response = new DefaultHttpClient().execute(postRequest);
//            is = response.getEntity().getContent();
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return convertStreamToString(is);
//    }

//    public String delete(String url, String contentType) throws ApiException {
//        if (!isOnline())
//            throw new ApiException(null, ApiException.ERROR_NO_INTERNET);
//
//        String result = "";
//        try {
//            HttpDelete httppost = new HttpDelete(url);
//            httppost.setHeader("Content-Type", contentType);
//            httppost.setHeader("token", ZabkaApp.getUserIdentifier());
//
//            HttpResponse response = new DefaultHttpClient().execute(httppost);
//
//            InputStream in = response.getEntity().getContent();
//
//            result = new Scanner(in).useDelimiter("\\A").next();
//
//        } catch (UnsupportedEncodingException e) {
//            throw new ApiException(e.getMessage());
//        } catch (ClientProtocolException e) {
//            throw new ApiException(e.getMessage());
//        } catch (IOException e) {
//            throw new ApiException(e.getMessage());
//        }
//
//        return result;
//    }

    public String post(String url, Map<String, String> post) throws ApiException {
        return post(url, post, "text/html;charset=UTF-8");
    }

    /**
     * Log. Comment to disable logs.
     *
     * @param msg
     */
    private void log(String msg) {
        log(msg, false);
    }

    private void log(String msg, boolean show) {
        if (show) {
            LOG.d(msg);
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8192);
        } catch (UnsupportedEncodingException e1) {
            LOG.e(e1);
        }
        StringBuilder string_builder = new StringBuilder(8192);

        String tmp = "";
        try {
            while ((tmp = reader.readLine()) != null)
                string_builder.append(tmp);
        } catch (IOException e) {
            LOG.e(e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                LOG.e(e);
            }
        }
        return string_builder.toString();
    }

//    public static boolean isOnline() {
//        ConnectivityManager cm = (ConnectivityManager) ZabkaApp.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo;
//        netInfo = cm.getActiveNetworkInfo();
//        if (netInfo != null && netInfo.isConnectedOrConnecting())
//            return true;
//        return false;
//    }

    private static HttpParams prepareBasicHttpParams() {
        HttpParams params = new BasicHttpParams();
        params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
        params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, HTTP.UTF_8);
        params.setParameter(CoreProtocolPNames.USER_AGENT, USER_AGENT_CHROME);
        params.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, SERVER_DELAY);
        params.setParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false);

        return params;
    }

    public class NotInitializedException extends Exception {

        private static final long serialVersionUID = 731794909017989799L;

        public NotInitializedException() {
            super();
        }

        public NotInitializedException(String err) {
            super(err);
        }
    }
}