package com.example.api;

import android.content.Context;
import com.example.RegistrationData;
import com.example.api.http.Http;
import com.example.log.Logger;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Class for calling API methods. Used for: <br/>
 * <ul>
 * <li>
 * getting data from the server,
 * </li>
 * <li>
 * sending data to the server,
 * </li>
 * <li>
 * logging in/out a user,
 * </li>
 * <li>
 * registering a user.
 * </li>
 * </ul>
 *
 * @author Krzysztof Wagner
 */
public class Api {

    public static final String TRUE_VALUE = "1";

    public static final String FALSE_VALUE = "0";

    public static final String RESPONSE_STATUS_OK = "ok";

    public static final String SETTINGS_SP_KEY = "sharedPrefsSettingsKey";

    @SuppressWarnings("unused")
    private static final String TAG = Api.class.getSimpleName();

    private final Logger LOG = new Logger(Api.class);

    private static Api INSTANCE;

    private Context      mContext;
    private Http mHttp;
    private ObjectMapper mMapper;

    public static Api getInstance(Context context) {
        return INSTANCE == null ? INSTANCE = new Api(context) : INSTANCE;
    }

    private static Executor EXECUTOR;

    public static Executor getExecutor() {
        return EXECUTOR == null ? Executors.newCachedThreadPool() : EXECUTOR;
    }

    private Api(Context context) {
        mContext = context;
        mHttp = Http.initialize(mContext);
        mMapper = initJsonMapper();
    }

    /**
     * Initializes ObjectMapper from Jackson library for the JSON objects mapping
     *
     * @return ObjectMapper without sensitive for unknown properties
     */
    private ObjectMapper initJsonMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return mapper;
    }

//    public void updateBanners() throws ApiException {
//        updateBasicDataFromApi("Banner?token=".concat(ZabkaApp.getUserIdentifier()), BannerTable.getInstance());
//    }
//
    private String getApiBaseUrl(String urlSuffix) {
        return "https://app.zabka.pl/pl/api/1/".concat(urlSuffix);
    }

    /**
     * Logs in a user. Gets a user token when logging in is successful.
     *
     * @return access user token
     * @throws ApiException
     */
    public String login(RegistrationData data, String token) throws ApiException {
        String url = getApiBaseUrl("User/login").concat("?token=")
                .concat(token);
        String response = mHttp.post(url, data, Http.CONTENT_TYPE_URL_ENCODED);
        LOG.d(response);

        return response;
    }

//    public void sendStampActivity(StampActivity stampActivity, Map<String, String> stringStringMap) throws ApiException {
//        String url = getApiBaseUrl("StampActivity/" + stampActivity.getId())
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.post(url, (Map<String, String>) stringStringMap, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        checkStatus(response);
//    }
//
//    public void addNewKeyRing(String name, String number, String pin) throws ApiException {
//        String url = getApiBaseUrl("KeyRing").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        PostParams post = new PostParams();
//        post.addParam(RegisterField.PENDANT_NAME.getApiName(), name);
//        post.addParam(RegisterField.PENDANT_NUMBER.getApiName(), number);
//        post.addParam(RegisterField.PENDANT_PIN.getApiName(), pin);
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        checkStatus(response);
//    }
//
//    /**
//     * Logs in a user using Facebook id and token. Gets a user token when logging in is successful.
//     *
//     * @return if found user on server by facebook id return his access token
//     * or if not found throw ApiException with error
//     * @throws ApiException
//     */
//
//    public String loginWithFacebook(String fb_id, String fb_token) throws ApiException {
//        String url = getApiBaseUrl("User/fblogin").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        LOG.d("trying login by facebook " + url);
//        Map<String, String> post = new HashMap<String, String>();
//        post.put("fb_id", fb_id);
//        post.put("fb_token", fb_token);
//        post.put(RegistrationData.UDID, ZabkaApp.getDeviceIdentifier());
//        post.put(RegistrationData.PLATFORM, RegistrationData.PLATFORM_ANDROID);
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//
//        return getUserToken(response);
//    }
//
//    public String logout() throws ApiException {
//        String url = getApiBaseUrl("User/logout").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        RegistrationData post = new RegistrationData();
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//
//        LOG.d(response);
//        return getUserToken(response);
//    }
//
//    /**
//     * Registers a new user on the server.
//     *
//     * @param data HashMap with user data
//     * @return id
//     * @throws ApiException
//     */
//    public String register(RegistrationData data) throws ApiException {
//        String url = getApiBaseUrl("User").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        LOG.d("register with data " + data);
//        String response = mHttp.post(url, data, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        return getUserId(response);
//    }
//
//    public String registerWithKik(RegistrationData data) throws ApiException {
//        String url = getApiBaseUrl("User/kikregister").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        LOG.d("register with data " + data);
//        String response = mHttp.post(url, data, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        return getUserToken(response);
//    }
//
//    public User verifyKik(RegistrationData data) throws ApiException {
//        String url;
//        url = getApiBaseUrl("User/verifykik").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        data.put(RegistrationData.UDID, ZabkaApp.getDeviceIdentifier());
//        data.put(RegistrationData.PLATFORM, RegistrationData.PLATFORM_ANDROID);
//        String response = mHttp.post(url, data, Http.CONTENT_TYPE_URL_ENCODED);
//
//        LOG.d(response);
//        checkStatus(response);
//        try {
//            UserSingleWrapper wrapper = mMapper.readValue(response, UserSingleWrapper.class);
//            return wrapper.getData();
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return null;
//    }
//
//    public void shopProposal(Map<String, String> shop, LatLng position, File file, int heightImage, int widthImage) throws ApiException {
//        String url = getApiBaseUrl("ShopProposal").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//        if (file != null) {
//            byte[] imageBytes = PhotoUtil.getRotateBitmap(file, heightImage, widthImage);
//            String baseName = "zabka_" + System.currentTimeMillis() + ".jpg";
//            ByteArrayBody bab = new ByteArrayBody(imageBytes, baseName);
//            reqEntity.addPart("image", bab);
//        }
//        shop.put(ShopProposal.LAT, position.latitude + "");
//        shop.put(ShopProposal.LNG, position.longitude + "");
//        for (Map.Entry<String, String> entry : shop.entrySet()) {
//            String key = entry.getKey();
//            String value = entry.getValue();
//            try {
//                StringBody stringBody = new StringBody(value);
//                reqEntity.addPart(key, stringBody);
//            } catch (UnsupportedEncodingException e) {
//                LOG.e(e);
//            }
//        }
//
//        String response = mHttp.multipartPost(url, reqEntity);
//        LOG.d(response);
//        checkStatus(response);
//    }
//
//
//    public void removeAccount(String userId) throws ApiException {
//        String url = getApiBaseUrl("User/" + userId).concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.delete(url, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//    }
//
//    /**
//     * Get a user data from the server. Access token is required.
//     *
//     * @return User Object has all available fields
//     * @throws ApiException
//     * @throws java.io.IOException
//     */
//    public User getUserData() throws ApiException, IOException {
//        String url = getApiBaseUrl("User").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//            JSONArray objects = getDataArrayFromResponse(response);
//            if (objects != null && objects.length() > 0) {
//                return mMapper.readValue(objects.getJSONObject(0).toString(), User.class);
//            }
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        return null;
//    }
//
//    public List<KeyRing> getKeyRings() throws ApiException, IOException {
//        String url = getApiBaseUrl("KeyRing").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//            JSONArray objects = getDataArrayFromResponse(response);
//            if (objects != null && objects.length() > 0) {
//
//                KeyRingsWrapper wrapper = mMapper.readValue(response, KeyRingsWrapper.class);
//                return wrapper.getData();
//            }
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        return new ArrayList<KeyRing>();
//    }
//
//    public void deleteKeyRings(String keyRingId) throws ApiException {
//        String url = getApiBaseUrl("KeyRing/" + keyRingId)
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.delete(url, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//
//        JSONObject jsonResponse = null;
//        try {
//            jsonResponse = new JSONObject(response);
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        checkStatus(jsonResponse); // Throws ApiException when errors
//        // occurred
//
//
//    }
//
//    public List<PromotionType> updatePromotionTypes() throws ApiException {
//        String url = getApiBaseUrl("PromotionType")
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        ;
//        String response = mHttp.get(url, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        List<PromotionType> result = new ArrayList<PromotionType>();
//        PromotionTypeTransformer transformer = new PromotionTypeTransformer();
//        try {
//            PromotionTypeWrapper wrapper = mMapper.readValue(response, PromotionTypeWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//
//    public List<Avatar> getAvatarList() throws ApiException {
//        String url = getApiBaseUrl("Avatar")
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url, Http.CONTENT_TYPE_URL_ENCODED);
//        List<Avatar> result = new ArrayList<Avatar>();
//        AvatarTransformer transformer = new AvatarTransformer();
//        try {
//            AvatarWraper wrapper = mMapper.readValue(response, AvatarWraper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//    public List<PromotionCategory> updatePromotionCategories() throws ApiException {
//        String url = getApiBaseUrl("PromotionCategory")
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        List<PromotionCategory> result = new ArrayList<PromotionCategory>();
//        PromotionCategoryTransformer transformer = new PromotionCategoryTransformer();
//        try {
//            PromotionCategoryWrapper wrapper = mMapper.readValue(response, PromotionCategoryWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//
//    /**
//     * Updates a user data on the server. Access token is required.
//     *
//     * @param userId
//     * @return if update was successful return true
//     * @throws ApiException
//     */
//    public User updateUserData(MultipartEntity reqEntity, String userId) throws ApiException {
//        String url = getApiBaseUrl("User/" + userId)
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.multipartPost(url, reqEntity);
//        LOG.d(response);
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//            JSONArray objects = getDataArrayFromResponse(response);
//            if (objects != null && objects.length() > 0) {
//                return mMapper.readValue(objects.getJSONObject(0).toString(), User.class);
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        return null;
//    }
//
//    public User updateUserData(RegistrationData data, String userId) throws ApiException {
//        String url = getApiBaseUrl("User/" + userId)
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.post(url, data, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//            JSONArray objects = getDataArrayFromResponse(response);
//            if (objects != null && objects.length() > 0) {
//                return mMapper.readValue(objects.getJSONObject(0).toString(), User.class);
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        return null;
//    }
//
//    /**
//     * Sends a password recovery request to the server. If given email is not found on the server, throws ApiException with error.
//     *
//     * @return if reset was successful return true
//     * @throws ApiException
//     */
//    public boolean recoverPassword(RegistrationData data) throws ApiException {
//        String url = getApiBaseUrl("User/recoverpassword").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.post(url, data, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//        } catch (JSONException e) {
//            LOG.e(e);
//            return false;
//        }
//
//        return true;
//    }
//
//    public List<StampActivity> updateStampActivities() throws ApiException {
//        String url = getApiBaseUrl("StampActivity").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        List<StampActivity> result = new ArrayList<StampActivity>();
//        StampActivityTransformer transformer = new StampActivityTransformer();
//        LOG.d(response);
//        try {
//            StampActivityWrapper wrapper = mMapper.readValue(response, StampActivityWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//
//    public List<Info> updateInformations() throws ApiException {
//        List<Info> result = new ArrayList<Info>();
//        String url = getApiBaseUrl("Page").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        LOG.d("GET rules url: " + url);
//        String response = mHttp.get(url);
//        InfoTransformer transformer = new InfoTransformer(mContext);
//        LOG.d(response);
//        try {
//            PageCollectionWrapper wrapper = mMapper.readValue(response, PageCollectionWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//    public List<PromotionProduct> updatePromotionProducts() throws ApiException {
//        List<PromotionProduct> result = new ArrayList<PromotionProduct>();
//        String url = getApiBaseUrl("PromotionProduct").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        LOG.d("get promotion products url: " + url);
//        String response = mHttp.get(url);
//        PromotionProductTransformer transformer = new PromotionProductTransformer();
//        LOG.d(response);
//        try {
//            PromotionProductsWrapper wrapper = mMapper.readValue(response, PromotionProductsWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//
//    public List<StampAction> getStampActionList() throws ApiException {
//        List<StampAction> result = new ArrayList<StampAction>();
//        String url = getApiBaseUrl("StampAction").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//        try {
//            List<DatabaseRow> rows = translateRespanseForDataBaseRows(response, StampActionTable.getInstance());
//            removeOldRecord(rows, StampActionTable.getInstance());
//            for (DatabaseRow row : rows) {
//                result.add((StampAction) row);
//                row.save();
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//    public List<StampPrice> getStampPricesList() throws ApiException {
//        List<StampPrice> result = new ArrayList<StampPrice>();
//        String url = getApiBaseUrl("StampPrice").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//        StampPriceTransformer transformer = new StampPriceTransformer();
//        LOG.d(response);
//        try {
//            StampPriceWrapper wrapper = mMapper.readValue(response, StampPriceWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//
//    public List<CodeStampAwardItem> getCodeStampAwardItems() throws ApiException {
//        List<CodeStampAwardItem> result = new ArrayList<CodeStampAwardItem>();
//        String url = getApiBaseUrl("CodeStampAwardItem").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        CodeStampAwardItemTransformer transformer = new CodeStampAwardItemTransformer();
//        LOG.d(response);
//        try {
//            CodeStampAwardItemWrapper wrapper = mMapper.readValue(response, CodeStampAwardItemWrapper.class);
//            result = transformer.fromServerObjects(wrapper.getData());
//            for (CodeStampAwardItem row : result) {
//                row.save();
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//
//
//    public void getDashboardBg() throws ApiException {
//        String url = mContext.getString(R.string.api_base_url).concat("Settings?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        JsonNode rootNode;
//        try {
//            rootNode = mMapper.readTree(response);
//            JsonNode nameNode = rootNode.path("data");
//            Iterator<JsonNode> it = nameNode.elements();
//            while (it.hasNext()) {
//                Iterator<Map.Entry<String, JsonNode>> fieldsIt = it.next().fields();
//                while (fieldsIt.hasNext()) {
//                    Map.Entry<String, JsonNode> value = fieldsIt.next();
//                    if (value.getKey().equals("dashboard_anim_image_wide")) {
//                        String imageUrl = getImageDtoFromRespanse(value).getImageUrl();
//                        SharedPrefsHelper.getInstance(mContext).setStringToSave(SETTINGS_SP_KEY, imageUrl);
//                        SharedPrefsHelper.getInstance(mContext).save();
//                    }
//                }
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//
//    }
//
//    public List<StampAward> getStampAwardList() throws ApiException {
//        List<StampAward> result = new ArrayList<StampAward>();
//        String url = getApiBaseUrl("StampAward").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//        try {
//            List<DatabaseRow> rows = translateRespanseForDataBaseRows(response, StampAwardTable.getInstance());
//            removeOldRecord(rows, StampAwardTable.getInstance());
//            StampAwardTable.getInstance().deleteAll();
//            for (DatabaseRow row : rows) {
//                result.add((StampAward) row);
//                row.save();
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//    public List<StampAwardPrice> getStampAwardPriceList(String awardId) throws ApiException {
//        List<StampAwardPrice> result = new ArrayList<StampAwardPrice>();
//        String url = getApiBaseUrl("StampAwardPrice").concat("?stampaward_id=" + awardId)
//                .concat("&token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//        try {
//            List<DatabaseRow> rows = translateRespanseForDataBaseRows(response, StampAwardPriceTable.getInstance());
//            removeOldRecord(rows, StampAwardPriceTable.getInstance());
//            for (DatabaseRow row : rows) {
//                result.add((StampAwardPrice) row);
//                row.save();
//            }
//        } catch (IOException e) {
//            LOG.e(e);
//        } catch (Exception e) {
//            LOG.e(e);
//        }
//        return result;
//    }
//
//    public void removeOldRecord(List<DatabaseRow> rows, DatabaseTable table) {
//        List<String> newIds = new ArrayList<String>();
//        for (DatabaseRow row : rows) {
//            newIds.add(row.getValueString("id"));
//        }
//        String ids = TextUtils.join(", ", newIds);
//        DatabaseManager.getDatabase().execSQL(String.format("DELETE FROM " + table.getName() + " WHERE ID NOT IN (%s);", ids));
//    }
//
//    public List<DatabaseRow> translateRespanseForDataBaseRows(String response, DatabaseTable table) throws IOException {
//        JsonNode rootNode = mMapper.readTree(response);
//        JsonNode nameNode = rootNode.path("data");
//        Iterator<JsonNode> it = nameNode.elements();
//        List<DatabaseRow> rows = new ArrayList<DatabaseRow>();
//        while (it.hasNext()) {
//            Iterator<Map.Entry<String, JsonNode>> fieldsIt = it.next().fields();
//            DatabaseRow row = table.doMakeRow();
//            while (fieldsIt.hasNext()) {
//                Map.Entry<String, JsonNode> value = fieldsIt.next();
//                if (value.getKey().equals("image")) {
//                    JsonNode node = value.getValue();
//                    if (StringUtils.isNotBlank(node.toString())) {
//                        ImageDto image = mMapper.readValue(node.toString(), ImageDto.class);
//                        row.setValue(value.getKey(), image.getImageUrl());
//                    }
//                } else {
//                    row.setValue(value.getKey(), value.getValue().textValue());
//                }
//            }
//            rows.add(row);
//        }
//        return rows;
//    }
//
//    public boolean isCurrentVersionIsSupported() {
//        boolean isSupported = true;
//        List<SupportedApp> list = null;
//        try {
//            String versionName = mContext.getPackageManager()
//                    .getPackageInfo(mContext.getPackageName(), 0).versionName;
//            String url = getApiBaseUrl("SupportedApp?platform=android&token=" + ZabkaApp.getUserIdentifier()+"&version="+versionName);
//            String response = mHttp.get(url);
//            SupportedAppWrapper wrapper = mMapper.readValue(response, SupportedAppWrapper.class);
//
//            list = wrapper.getData();
//            if (list != null && !list.isEmpty()) {
//                SupportedApp supportedApp = list.get(0);
//                isSupported = supportedApp.getVersion().equals(versionName);
//            }else{
//                isSupported=false;
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            LOG.e(e);
//        } catch (ApiException e) {
//            LOG.e(e);
//        } catch (JsonMappingException e) {
//            LOG.e(e);
//        } catch (JsonParseException e) {
//            LOG.e(e);
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//
//        return isSupported;
//    }
//
//    public ImageDto getImageDtoFromRespanse(Map.Entry<String, JsonNode> value) throws IOException {
//        JsonNode node = value.getValue();
//        ImageDto image = null;
//        if (StringUtils.isNotBlank(node.toString())) {
//            image = mMapper.readValue(node.toString(), ImageDto.class);
//        }
//        return image;
//    }
//
//    public List<DatabaseRow> updateNewspapers() throws ApiException {
//        return updateBasicDataFromApi("Newspaper?token=".concat(ZabkaApp.getUserIdentifier()), NewspaperTable.getInstance());
//    }
//
//    /**
//     * Gets a user coupons from the server and saves in local database. Access token is required.
//     *
//     * @throws ApiException
//     */
//    public List<CouponCode> updateCouponsCode() throws ApiException {
//        String url = getApiBaseUrl("CouponCode").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        LOG.d(response);
//
//        List<CouponCode> couponCodes = new ArrayList<CouponCode>();
//        try {
//            CouponCodeTransformer transformer = new CouponCodeTransformer();
//            CouponCodeWrapper wrapper = mMapper.readValue(response, CouponCodeWrapper.class);
//            couponCodes = transformer.fromServerObjects(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return couponCodes;
//    }
//
//    /**
//     * Shares a coupon for an other Zabka user by his email or phone number.
//     * When coupon is shared successfully, it is removed from local database.
//     * If the given email address or phone number in not found on the server, throws ApiException with an error message.
//     *
//     * @param couponCode for share
//     * @param login      email or phone number the user with whom you want to share
//     * @throws ApiException
//     */
//    public void shareCouponCode(CouponCode couponCode, String login) throws ApiException {
//        String url = getApiBaseUrl("CouponCode/share").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        Map<String, String> post = new HashMap<String, String>();
//        post.put("token", ZabkaApp.getUserIdentifier());
//        post.put("id", couponCode.getId());
//        post.put("login", login);
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        LOG.d("shareCouponCode", "response: " + response);
//    }
//
//    public boolean displayCouponCode(String id) throws ApiException {
//        String url = getApiBaseUrl("CouponCode/display")
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        Map<String, String> post = new HashMap<String, String>();
//        post.put("id", id);
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        checkStatus(response);
//
//
//        return true;
//
//    }
//    /**
//     * Shares a shopping list for an other Zabka user by his email or phone number.
//     * If the given email address or phone number in not found on the server, throws ApiException with an error message.
//     *
//     * @param shoppingList for share
//     * @param login        email or phone number the user with whom you want to share
//     * @throws ApiException
//     */
//    public void shareBuyList(ShoppingList shoppingList, String login) throws ApiException {
//        String url = getApiBaseUrl("BuyList/share").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        Map<String, String> post = new HashMap<String, String>();
//        post.put("token", ZabkaApp.getUserIdentifier());
//        post.put("id", shoppingList.getServerId());
//        post.put("login", login);
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//
//        LOG.d("Share BuyList: " + response);
//        checkStatus(response);
//
//        shoppingList.setShared(true);
//        shoppingList.setSyncState(SyncState.EDITED);
//        shoppingList.save();
//    }
//
//    /**
//     * Gets all the user shopping lists from the server. Access token is required.
//     *
//     * @return collection with shopping lists
//     * @throws ApiException
//     */
//    public List<ShoppingList> getBuyLists() throws ApiException {
//        String url = getApiBaseUrl("BuyList").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url);
//        ShoppingListTransformer transformer = new ShoppingListTransformer();
//        LOG.d(response);
//        try {
//            BuyListCollectionWrapper wrapper = mMapper.readValue(response, BuyListCollectionWrapper.class);
//            return transformer.fromServerObjs(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return new ArrayList<ShoppingList>();
//    }
//
//    /**
//     * Update or create Shopping List object on server.
//     * If Object has server id this method will update shopping list on server because object is exist
//     * else object will be create on server
//     *
//     * @param shoppingList
//     * @return
//     * @throws ApiException
//     */
//    public ShoppingList updateBuyList(ShoppingList shoppingList) throws ApiException {
//        String url;
//        if (shoppingList.hasServerId()) {
//            LOG.d("UPDATE BUY LIST");
//            url = getApiBaseUrl("BuyList/" + shoppingList.getServerId())
//                    .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        } else {
//            LOG.d("CREATE BUY LIST");
//            url = getApiBaseUrl("BuyList").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        }
//
//        ShoppingListTransformer transformer = new ShoppingListTransformer();
//        try {
//            String buyListInJson = mMapper.writeValueAsString(transformer.toServerObj(shoppingList));
//            LOG.d("trying create BuyList: " + buyListInJson);
//            String response = mHttp.post(url, buyListInJson, Http.CONTENT_TYPE_URL_ENCODED);
//            checkStatus(response);
//            LOG.d("Created BuyList: " + response);
//            BuyListSingleWrapper wrapper = mMapper.readValue(response, BuyListSingleWrapper.class);
//            return transformer.fromServerObj(wrapper.getData(), shoppingList.getId());
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return null;
//    }
//
//
//    /**
//     * Removes a shopping List from the server by shopping list id and user access token.
//     *
//     * @param shoppingList for remove
//     * @throws ApiException
//     */
//    public void removeBuyList(ShoppingList shoppingList) throws ApiException {
//        String url = getApiBaseUrl("BuyList/" + shoppingList.getServerId())
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.delete(url, Http.CONTENT_TYPE_URL_ENCODED);
//
//        LOG.d("Remove BuyList: " + response);
//        checkStatus(response);
//    }
//
//    /**
//     * Gets products belonging to the given shopping list.
//     *
//     * @return collection with Products
//     * @throws ApiException
//     */
//    public List<Product> getBuyItemsForList(String serverListId) throws ApiException {
//        String url = getApiBaseUrl("BuyItem").concat("?token=").concat(ZabkaApp.getUserIdentifier()).concat("&buylist_id=").concat(serverListId);
//        LOG.d("trying get buyItem for list by url: " + url);
//        String response = mHttp.get(url);
//        ProductTransformer transformer = new ProductTransformer();
//        LOG.d(response);
//        try {
//            BuyItemCollectionWrapper wrapper = mMapper.readValue(response, BuyItemCollectionWrapper.class);
//            return transformer.fromServerObjs(wrapper.getData());
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return new ArrayList<Product>();
//    }
//
//    /**
//     * Updates or creates given product on the server.
//     * If given product has server_id, this method will update the product on server. Otherwise a new object will be created on the server.
//     *
//     * @return
//     * @throws ApiException
//     */
//    public Product updateBuyItem(Product product) throws ApiException {
//        String url;
//        if (product.hasServerId()) {
//            LOG.d("UPDATE BUY ITEM");
//            url = getApiBaseUrl("BuyItem/" + product.getServerId())
//                    .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        } else {
//            LOG.d("CREATE BUY ITEM");
//            url = getApiBaseUrl("BuyItem").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        }
//
//        ProductTransformer transformer = new ProductTransformer();
//        try {
//            String buyItemInJson = mMapper.writeValueAsString(transformer.toServerObj(product));
//            LOG.d("trying update BuyItem: " + buyItemInJson);
//            String response = mHttp.post(url, buyItemInJson, Http.CONTENT_TYPE_URL_ENCODED);
//            checkStatus(response);
//            LOG.d("Update BuyList: " + response);
//            BuyItemSingleWrapper wrapper = mMapper.readValue(response, BuyItemSingleWrapper.class);
//            return transformer.fromServerObj(wrapper.getData(), product.getId());
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return null;
//    }
//
//
//    /*
//
//        /**
//         * Removes given product from the server. The access token is required.
//         *
//         * @param token
//         * @param product
//         * @throws ApiException
//         */
//    public void removeBuyItem(Product product) throws ApiException {
//        String url = getApiBaseUrl("BuyItem/" + product.getServerId())
//                .concat("/?token=").concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.delete(url, Http.CONTENT_TYPE_URL_ENCODED);
//
//        LOG.d("Remove BuyItem: " + response);
//        checkStatus(response);
//    }
//
//
//    /**
//     * Common method for queries to the server with automatic inserting retrieved objects into a local database.
//     *
//     * @param urlSuffix
//     * @param table
//     * @return list of stored objects in local database
//     * @throws ApiException
//     */
//    private List<DatabaseRow> updateBasicDataFromApi(String urlSuffix, DatabaseTable table) throws ApiException {
//        String url = getApiBaseUrl(urlSuffix);
//        return updateBasicDataFromApiForUrl(url, table);
//    }
//
//    public List<DatabaseRow> updateBasicDataFromApiForUrl(String url, DatabaseTable table) throws ApiException {
//        String response = mHttp.get(url);
//        try {
//            List<DatabaseRow> result = new ArrayList<DatabaseRow>();
//            JSONArray objects = getDataArrayFromResponse(response);
//
//            int objectsCount = objects.length();
////            LOG.d("updateBasicDataFromApi: " + objects + " objectsCount: " + objectsCount + " response:" + response);
//
//
//            for (int i = 0; i < objectsCount; i++) {
//                JSONObject jsonObject = objects.getJSONObject(i);
//                DatabaseRow dbObject = mapJsonObjectToDbObject(jsonObject, table);
//                dbObject.save();
//                result.add(dbObject);
//            }
//            return result;
//        } catch (JSONException e) {
//            LOG.e(e);
//            throw new ApiException(e.getMessage());
//        }
//    }
//
//    public void getStringTranslation() throws ApiException {
//        String url = getApiBaseUrl("translations").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier());
//        String response = mHttp.get(url, Http.CONTENT_TYPE_URL_ENCODED);
//        try {
//            JSONObject object = new JSONObject(response);
//            TranslationTable table = TranslationTable.getInstance();
//            if (table.getCount() > 0) {
//                table.deleteAll();
//            }
//            Translation row = table.doMakeRow();
//            mapJSonObjectToDbObject(getDataJSONFromResponse(object), row);
//            row.save();
//        } catch (JSONException e) {
//            throw new ApiException(e.getMessage());
//        }
//    }
//
//    private JSONObject getDataJSONFromResponse(JSONObject json) throws JSONException {
//        return json.optJSONObject("data");
//    }
//
//    private void mapJSonObjectToDbObject(JSONObject object, Translation row) {
//        RowData[] columns = row.getColumnList();
//        for (RowData column : columns) {
//            if (!("id").equals(column.getName())) {
//                String value = object.optString(column.getName(), "");
//                row.setValue(column, value);
//            }
//        }
//    }
//
//
//    /**
//     * Gets a user token from a server response.
//     *
//     * @param response
//     * @return access token
//     * @throws ApiException
//     */
//    private String getUserToken(String response) throws ApiException {
//        String token = null;
//        JSONObject jsonResponse;
//        try {
//            jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//            token = jsonResponse.optString("token");
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        return token;
//    }
//
//    /**
//     * Gets a user token from a server response.
//     *
//     * @param response
//     * @return access token
//     * @throws ApiException
//     */
//    private String getUserId(String response) throws ApiException {
//        String id = null;
//        JSONObject jsonResponse;
//        try {
//            jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse); // Throws ApiException when errors
//            // occurred
//            id = jsonResponse.optString("id");
//        } catch (JSONException e) {
//            LOG.e(e);
//        }
//        return id;
//    }
//
//    /**
//     * Check a status of the server response. If the status is other than 'OK' throws ApiException with an error message.
//     *
//     * @param response
//     * @throws ApiException
//     */
//    private void checkStatus(String response) throws ApiException {
//        try {
//            JSONObject jsonResponse = new JSONObject(response);
//            checkStatus(jsonResponse);
//        } catch (JSONException e) {
//            LOG.e(e);
//            throw new ApiException(response);
//        }
//    }
//
//    /**
//     * Check server response status from JsonRespone Object
//     * If the status is other than 'OK' throws ApiException with an error message.
//     *
//     * @param jsonResponse
//     * @throws ApiException
//     */
//    private void checkStatus(JSONObject jsonResponse) throws ApiException {
//        String status = jsonResponse.optString("status");
//        if (status != null) {
//            if (status.equals(Api.RESPONSE_STATUS_OK)) {
//                return;
//            }
//
//            String code = jsonResponse.optString("code");
//            String errorValue = jsonResponse.optString("error");
//            if (errorValue != null && errorValue.equals("Not Authorized")) {
//                Settings.flushUserToken();
//            }
//
//            JSONArray errors = jsonResponse.optJSONArray("errors");
//            if (errors == null) {
//                JSONObject error = jsonResponse.optJSONObject("errors");
//                if (error != null) {
//                    errors = new JSONArray();
//                    errors.put(error);
//                }
//            }
//            throw new ApiException(errors, code);
//        }
//    }
//
//
//    /**
//     * Retrieves all shops for the specified area of ​​map and active filter (cafe / bakery / lotto).
//     * Returns an empty collection if no shop is found.
//     *
//     * @param cordsData
//     * @return List with shops
//     * @throws ApiException
//     * @throws NotFoundNearstShopException
//     */
//    public List<Shop> getShopsBy(CordsData cordsData) throws ApiException {
//        List<Shop> shops = new ArrayList<Shop>();
//        String url = getApiBaseUrl("Shop").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier())
//                .concat("&")
//                .concat(cordsData.buildHttpParams());
//        LOG.d("Find Shops by url: " + url);
//        String response = mHttp.get(url);
//        checkStatus(response);
//        LOG.d("Find Shops: " + response);
//
//        try {
//            ShopCollectionWrapper wrapper = mMapper.readValue(response, ShopCollectionWrapper.class);
//            shops = wrapper.getData();
//        } catch (IOException e) {
//            LOG.e("can not parse response " + e.getMessage());
//        }
//        return shops;
//    }
//
//    /**
//     * Retrieves a nearest shop for the specified coordinates
//     * If no shop is found, throws a NotFoundNearstShopException.
//     *
//     * @param cordsData
//     * @return single Shop
//     * @throws ApiException
//     * @throws NotFoundNearstShopException
//     */
//    public Shop getNearestShopBy(CordsData cordsData) throws ApiException, NotFoundNearstShopException {
//        List<Shop> shops = new ArrayList<Shop>();
//        String url = getApiBaseUrl("Shop/nearestshop").concat("?token=")
//                .concat(ZabkaApp.getUserIdentifier())
//                .concat("&")
//                .concat(cordsData.buildHttpParams());
//        LOG.d("Find Shops by url: " + url);
//        String response = mHttp.get(url);
//        checkStatus(response);
//        LOG.d("Find Shops: " + response);
//
//        try {
//            ShopCollectionWrapper wrapper = mMapper.readValue(response, ShopCollectionWrapper.class);
//            shops = wrapper.getData();
//        } catch (IOException e) {
//            LOG.e("can not parse response " + e.getMessage());
//        }
//
//        if (CollectionUtils.isEmpty(shops)) {
//            throw new NotFoundNearstShopException("");
//        } else {
//            return shops.get(0);
//        }
//    }
//
//    /**
//     * Sends shop rate
//     *
//     * @param shopId - selected shop id
//     * @param rate   - rate from rating bar
//     * @return
//     */
//    public boolean sendShopRate(String shopId, int rate) throws ApiException {
//        String url = getApiBaseUrl("ShopRate").concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        Map<String, String> post = new HashMap<String, String>();
//        post.put("shop_id", shopId);
//        post.put("rate", rate + "");
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//
//        checkStatus(response);
//
//        return true;
//
//    }
//
//    public List<DailyCouponAction> getDailyCouponAction() throws ApiException {
//        List<DailyCouponAction> dailyCouponAction = new ArrayList<DailyCouponAction>();
//        String url = getApiBaseUrl("DailyCouponAction")
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        LOG.d("getDailyCouponAction URL: " + url);
//        String response = mHttp.get(url);
//        LOG.d(response);
//
//        checkStatus(response);
//
//        try {
//            DailyCouponActionWrapper wrapper = mMapper.readValue(response, DailyCouponActionWrapper.class);
//            dailyCouponAction = wrapper.getData();
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//        return dailyCouponAction;
//    }
//
//
//    public AssignedDailyCouponAction selectDailyCouponAction(String id) throws ApiException {
//        String url = getApiBaseUrl("DailyCouponAction/assigncoupon")
//                .concat("?token=").concat(ZabkaApp.getUserIdentifier());
//        Map<String, String> post = new HashMap<String, String>();
//        post.put("id", id);
//        String response = mHttp.post(url, post, Http.CONTENT_TYPE_URL_ENCODED);
//        LOG.d(response);
//        checkStatus(response);
//
//        try {
//            AssignedDailyCouponActionWrapper wrapper = mMapper.readValue(response, AssignedDailyCouponActionWrapper.class);
//            return wrapper.getData();
//        } catch (IOException e) {
//            LOG.e(e);
//        }
//
//        return null;
//
//    }
//
//    /**
//     * Translates Json object into database object. Translates only json fields
//     * corresponding with db object columns. Columns "image" and "cover" are
//     * mapped differently.
//     *
//     * @param object
//     * @param table
//     * @return
//     * @throws org.json.JSONException
//     */
//    private DatabaseRow mapJsonObjectToDbObject(JSONObject object, DatabaseTable table) throws JSONException {
//        List<String> imageMapping = new ArrayList<String>();
//        imageMapping.add("image");
//        imageMapping.add("cover");
//        return mapJsonObjectToDbObject(object, table, imageMapping);
//    }
//
//    /**
//     * Translates Json object into database object. Translates only json fields
//     * corresponding with db object columns.
//     *
//     * @param object
//     * @param table
//     * @return
//     * @throws org.json.JSONException
//     */
//    private DatabaseRow mapJsonObjectToDbObject(JSONObject object, DatabaseTable table, List<String> imageMapping)
//            throws JSONException {
//        DatabaseRow dbObject = table.doMakeRow();
//
//        RowData[] columns = dbObject.getColumnList();
//        for (RowData column : columns) {
//            if (imageMapping.contains(column.getName())) {
//                String imageUrl = mapImageUrl(object, column.getName());
//                dbObject.setValue(column, imageUrl);
//            } else {
//                dbObject.setValue(column, object.optString(column.getName(), ""));
//            }
//        }
//
//        return dbObject;
//    }
//
//    private String mapImageUrl(JSONObject object, String key) throws JSONException {
//        String imageUrl = "";
//        JSONObject image = object.optJSONObject(key);
//        if (image != null) {
//            imageUrl = image.getJSONObject("default").getString("url");
//        }
//        return imageUrl;
//    }
//
//    private JSONObject getDataObjectFromResponse(String response) throws JSONException {
//        JSONObject jsonResponse = new JSONObject(response);
//        JSONObject dataObject = jsonResponse.optJSONObject("data");
//        return dataObject;
//    }
//
//    private JSONArray getDataArrayFromResponse(String response) throws JSONException {
//        JSONObject jsonResponse = new JSONObject(response);
//        JSONObject dataObject = jsonResponse.optJSONObject("data");
//        JSONArray objects;
//        if (dataObject != null) {
//            objects = new JSONArray();
//            objects.put(dataObject);
//        } else {
//            objects = jsonResponse.getJSONArray("data");
//        }
//        return objects;
//    }
//
//    /**
//     * Gets cover and pages (urls) for a newspaper/catalog from server and joins with existing newspaper in local database.
//     *
//     * @param newspaper
//     * @throws ApiException
//     */
//    public void updateNewspaperContent(Newspaper newspaper) throws ApiException {
//        String urlSuffix = "ArImage?object_id=".concat(newspaper.getId()).concat(
//                "&object_class=Newspaper&type_in=pages,cover&token=").concat(ZabkaApp.getUserIdentifier());
//        String url = mContext.getString(R.string.api_base_url).concat(urlSuffix);
//        String response = mHttp.get(url);
//        try {
//            JSONArray objects = getDataArrayFromResponse(response);
//            int objectsCount = objects.length();
//            List<String> positions = new ArrayList<String>();
//            Map<String, String> urls = new HashMap<String, String>();
//            for (int i = 0; i < objectsCount; i++) {
//                JSONObject jsonObject = objects.getJSONObject(i);
//                String position = jsonObject.optString("position");
//                String imageUrl = jsonObject.optJSONObject("default").optString("url");
//                positions.add(position);
//                urls.put(position, imageUrl);
//            }
//            Collections.sort(positions);
//            String pages = "";
//            for (String pos : positions) {
//                pages = pages.concat(urls.get(pos)).concat(";");
//            }
//            // Cut off last ';'
//            if (pages.length() > 0) {
//                pages = pages.substring(0, pages.length() - 1);
//            }
//            newspaper.setValue(Newspaper.pages, pages);
//            newspaper.save();
//        } catch (JSONException e) {
//            LOG.e(e);
//            throw new ApiException(e.getMessage());
//        }
//    }
}