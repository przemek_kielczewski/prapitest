package com.example;


import java.util.HashMap;

/**
 * Its HashMap used in user registration
 * key - api key name (from RegistrationData fields)
 * value - is strings from registration form (in boolean values put strings: 0 or 1)
 */
public class RegistrationData extends HashMap<String, String> {

    public static final String LOGIN                        = "login";
    public static final String FIRST_NAME                   = "first_name";
    public static final String LAST_NAME                    = "last_name";
    public static final String EMAIL                        = "email";
    public static final String PHONE                        = "phone";
    public static final String PASSWORD                     = "password";
    public static final String PASSWORD_OLD                 = "old_password";
    public static final String PASSWORD_CONFIRM             = "password_confirm";
    public static final String AGE                          = "age";
    public static final String PLATFORM                     = "platform";
    public static final String UDID                         = "udid";
    public static final String KIK                          = "kik";
    public static final String TOKEN                        = "token";
    public static final String PESEL                        = "pesel";
    public static final String NIP                          = "nip";
    public static final String STREET                       = "street";
    public static final String STREET_NUMBER                = "street_number";
    public static final String POST_CODE                    = "post_code";
    public static final String CITY                         = "city";
    public static final String OPERATOR                     = "operator";
    public static final String OPERATOR_RAW                 = "operator_raw";
    public static final String WANT_KIK                     = "obtain_kik";
    public static final String SEX                          = "sex";
    public static final String LOCATION                     = "location";
    public static final String BIRTH_DATE                   = "dob";
    public static final String ACCEPT_ALCOHOL               = "accept_alcohol";
    public static final String ACCEPT_COMMERCIAL_SERVICES   = "accept_rules";
    public static final String ACCEPT_MARKETING_DATA        = "accept_processing";
    public static final String ACCEPT_APPLICATION_RULES     = "accept_application_rules";
    public static final String ACCEPT_MONEY_MEDIATION_RULES = "money_mediation";


    public static final String PLATFORM_ANDROID = "android";

    public RegistrationData() {
        put(PLATFORM, PLATFORM_ANDROID);
        put(UDID, "1");
        put(OPERATOR, "PLUS");
    }
}
