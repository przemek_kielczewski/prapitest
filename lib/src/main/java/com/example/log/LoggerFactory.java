package com.example.log;

/**
 * @author Mateusz Strzelecki
 * @since 1.0.0
 */
public class LoggerFactory {

    public static Logger getInstance(Class<?> clz) {
        return new Logger(clz);
    }

}