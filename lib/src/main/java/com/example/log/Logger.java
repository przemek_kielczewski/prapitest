package com.example.log;

import android.util.Log;

/**
 * @author Mateusz Strzelecki
 * @since 1.0.0
 */

public final class Logger {

    private final String tag;

    public Logger(Class<?> clz) {
        this.tag = clz.getSimpleName();
    }

    public void d(String message) {
//        Log.d(tag, message);
    }

    public void d(String message, Object... args) {
        Log.d(tag, String.format(message, args));
    }

    public void e(Throwable throwable, String message, Object... args) {
//        Log.e(tag, String.format(message, args), throwable);
    }

    public void e(String message, Object... args) {
//        Log.e(tag, String.format(message, args));
    }

    public void e(Throwable throwable) {
//        Log.e(tag, "", throwable);
    }

}